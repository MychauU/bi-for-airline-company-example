/*drop database hd_74
/create database hd_74
*/
use hd_74

DROP TABLE Operacja
DROP TABLE Osoba
DROP TABLE Typ_operacji
DROP TABLE Samolot_realizujacy
DROP TABLE Realizacja_lotu
DROP TABLE Lot
DROP TABLE Port_lotniczy
DROP TABLE Samolot
DROP TABLE Czas
DROP TABLE Data
DROP TABLE Samolot_Realizujacy_status
DROP TABLE Realizacja_lotu_status

CREATE TABLE Port_lotniczy
(
	ID_portu integer IDENTITY(1,1) PRIMARY KEY,
	Nazwa varchar(80)  ,
	Miejscowosc varchar(40)  ,
	Kraj varchar(40)  ,
	Strefa_czasowa varchar(15)  
);

CREATE TABLE Lot
(
	ID_lotu integer IDENTITY(1,1) PRIMARY KEY,
	ID_port_skad integer  REFERENCES Port_lotniczy(ID_portu),
	ID_port_dokad integer  REFERENCES Port_lotniczy(ID_portu),
	rodzaj_odleglosci varchar(20)  
	
);

CREATE TABLE Samolot
(
	ID_samolotu integer IDENTITY(1,1) PRIMARY KEY,
	Model varchar(20)  ,
	Nazwa_seryjna varchar(20) UNIQUE  ,
);


CREATE TABLE Czas
(
	ID_czasu integer IDENTITY(1,1) PRIMARY KEY,
	Godzina varchar (10)  NULL,
	Minuta varchar(10)  NULL,
	Pora_dnia varchar(15)  NULL,
);

CREATE TABLE Data
(
	ID_daty integer IDENTITY(1,1) PRIMARY KEY,
	data date NULL,
	rok varchar(10) NULL,
	miesiac varchar(15) NULL,
	dzien_tygodnia varchar(15) NULL
);

CREATE TABLE Samolot_realizujacy_status
(
	ID_statusu integer IDENTITY(1,1) PRIMARY KEY,
	status varchar(20)  ,
);

CREATE TABLE Realizacja_lotu_status
(
	ID_statusu integer IDENTITY(1,1) PRIMARY KEY,
	status varchar(20)  ,
);

CREATE TABLE Realizacja_lotu
(
	ID_wyk_lotu integer IDENTITY(1,1) PRIMARY KEY,
	ID_lotu integer   REFERENCES Lot(ID_lotu),
	ID_data_dodania integer   REFERENCES Data(ID_daty),
	ID_czas_dodania integer   REFERENCES Czas(ID_czasu),
	ID_data_planowanego_startu integer   REFERENCES Data(ID_daty),
	ID_czas_planowanego_startu integer   REFERENCES Czas(ID_czasu),
	ID_data_planowanego_ladowania integer   REFERENCES Data(ID_daty),
	ID_czas_planowanego_ladowania integer   REFERENCES Czas(ID_czasu),
	ID_data_startu integer   REFERENCES Data(ID_daty),
	ID_czas_startu integer   REFERENCES Czas(ID_czasu),
	ID_data_ladowania integer   REFERENCES Data(ID_daty),
	ID_czas_ladowania integer   REFERENCES Czas(ID_czasu),
	ID_statusu integer   REFERENCES Realizacja_lotu_status(ID_statusu),
	Ilosc_zajetych_miejsc integer,
	Max_miejsc integer,
	Ilosc_zuzytego_paliwa integer,
	Koszt_wykonania_lotu integer,
	dystans_przebyty integer,
	
);

CREATE TABLE Samolot_realizujacy
(
	ID_samolotu_realizujacego integer IDENTITY(1,1) PRIMARY KEY,
	ID_samolotu integer   REFERENCES Samolot(ID_samolotu),
	ID_realizacji_lotu integer   REFERENCES Realizacja_lotu(ID_wyk_lotu),
	ID_data_nadania_samolotu integer   REFERENCES Data(ID_daty),
	ID_czas_nadania_samolotu integer   REFERENCES Czas(ID_czasu),
	ID_statusu integer   REFERENCES Samolot_realizujacy_status(ID_statusu),
);

CREATE TABLE Osoba
(
	ID_osoby integer identity(1,1) PRIMARY KEY,
	ID_uzytkownika integer REFERENCES Osoba(ID_osoby),
	Data_dolaczenia date   ,
	Data_wygasniecia date  ,
	Data_ostatniego_lotu date ,
	Imie_nazwisko varchar(40)  ,
	kod_identyfikujacy varchar(15)   ,
	Typ_dokumentu varchar(20)  ,
	Numer_dokumentu varchar(20)  
);

CREATE TABLE Typ_operacji
(
	ID_typ_operacji integer IDENTITY(1,1) PRIMARY KEY,
	nazwa_operacji varchar(40)  
);

CREATE TABLE Operacja
(
	ID_operacji integer IDENTITY(1,1) PRIMARY KEY,
	ID_kupujacego integer  REFERENCES Osoba(ID_osoby),
	ID_wlasciciela integer   REFERENCES Osoba(ID_osoby),
	ID_realizacji_lotu integer   REFERENCES Realizacja_lotu(ID_wyk_lotu),
	ID_typ_operacji integer   REFERENCES Typ_operacji(ID_typ_operacji),
	ID_data_dodania integer   REFERENCES Data(ID_daty),
	ID_czas_dodania integer   REFERENCES Czas(ID_czasu),
	Numer_transakcji varchar(15)  ,
	Cena_operacji integer
	
);





