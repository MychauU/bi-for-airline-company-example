/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carried_flight;

import flight.Flight;
import flight.Flights;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import utils.ExpodentialDistributionRandom;
import utils.StaticFunctions;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class CarriedFlights {

    public Map<Long, CarriedFlight> carriedFlights;
    Flights flights;
    long counter;
    ExpodentialDistributionRandom expodentialRandom;

    public CarriedFlights(Flights flights) {
        expodentialRandom = new ExpodentialDistributionRandom();
        counter = 1;
        carriedFlights = new TreeMap<>();
        this.flights = flights;
    }

    public void loadCarriedFlights(long quantity, Calendar start, Calendar end) {
        for (int i = 0; i < quantity;) {
            Flight pomFlight;
            pomFlight = this.flights.flights.get(StaticFunctions.nextLong(this.flights.flights.size())+1);

            long pom = expodentialRandom.nextLong(end.getTimeInMillis() - start.getTimeInMillis()+1) + start.getTimeInMillis();
            Date pomDate = new Date(pom);
            Calendar calendarLoadingDate = Calendar.getInstance();
            calendarLoadingDate.setTime(pomDate);

            Calendar scheduledCalendarStart = Calendar.getInstance();
            scheduledCalendarStart.setTime(calendarLoadingDate.getTime());
            scheduledCalendarStart.set(Calendar.MONTH, 6);
            scheduledCalendarStart.set(Calendar.HOUR_OF_DAY, StaticVariables.random.nextInt(24));
            scheduledCalendarStart.set(Calendar.MINUTE, StaticVariables.random.nextInt(60));

            Calendar scheduledCalendarEnd = Calendar.getInstance();
            scheduledCalendarEnd.setTime(scheduledCalendarStart.getTime());

            //add 1 hour for getting to plane and going off the plane
            scheduledCalendarEnd.add(Calendar.HOUR_OF_DAY, 1);

            //plane 800 km/h
            int timeOfFlight=(int) (Math.ceil( (double) ((double) pomFlight.distance) / ((double) 800.0) ) );
            scheduledCalendarEnd.add(Calendar.HOUR_OF_DAY,   timeOfFlight);
            scheduledCalendarEnd.add(Calendar.MINUTE, StaticVariables.random.nextInt(60));

            Calendar calendarStart = Calendar.getInstance();
            calendarStart.setTime(scheduledCalendarStart.getTime());

            Calendar calendarEnd = Calendar.getInstance();
            calendarEnd.setTime(scheduledCalendarEnd.getTime());

            calendarStart.add(Calendar.MINUTE, StaticVariables.random.nextInt(20));
            calendarEnd.add(Calendar.MINUTE, StaticVariables.random.nextInt(20));

            int chanceToDelay = StaticVariables.random.nextInt(1000);
            String pomStatus = "Wykonano";
            
            //0,5% chance to not execute
            if (chanceToDelay < 5) {
                pomStatus = "Nie wykonano";
            }
            //3% chance to delay
            else if (chanceToDelay < 30) {
                calendarStart.add(Calendar.HOUR_OF_DAY, StaticVariables.random.nextInt(6));
                calendarEnd.add(Calendar.MINUTE, StaticVariables.random.nextInt(60));
                calendarStart.add(Calendar.HOUR_OF_DAY, StaticVariables.random.nextInt(6));
                calendarEnd.add(Calendar.MINUTE, StaticVariables.random.nextInt(60));
                pomStatus = "Opóźniony";
            }


            CarriedFlight pomCarriedFlight;
            pomCarriedFlight = new CarriedFlight(counter, pomFlight.id, calendarStart.getTime(), calendarEnd.getTime(), scheduledCalendarStart.getTime(), scheduledCalendarEnd.getTime(), calendarLoadingDate.getTime(), pomStatus,pomFlight);
            carriedFlights.put(counter, pomCarriedFlight);
            counter++;
            i++;
        }
    }
}
