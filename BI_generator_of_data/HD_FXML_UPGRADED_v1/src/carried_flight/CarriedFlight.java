/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carried_flight;

import client.Passager;
import flight.Flight;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author MychauU
 */
public class CarriedFlight {
    public long id;
    public long flightId;
    public Date flightStart;
    public Date flightEnd;
    public Date scheduledFlightStart;
    public Date scheduledFlightEnd;
    public Date loadingDate;
    public String status;
    public Flight flightWsk;
    public List<Passager> passagerList;
    public boolean done;
    /**
     * 
     * @param id
     * @param flightId
     * @param flightStart
     * @param flightEnd
     * @param scheduledFlightStart
     * @param scheduledFlightEnd
     * @param loadingDate
     * @param status 
     */
    public CarriedFlight(long id,long flightId,Date flightStart,Date flightEnd,Date scheduledFlightStart,Date scheduledFlightEnd,Date loadingDate,String status, Flight flightWsk){
        this.id=id;
        this.flightId=flightId;
        this.flightStart=flightStart;
        this.flightEnd=flightEnd;
        this.scheduledFlightEnd=scheduledFlightEnd;
        this.scheduledFlightStart=scheduledFlightStart;
        this.loadingDate=loadingDate;
        this.status=status;
        this.flightWsk=flightWsk;
        passagerList=new ArrayList<>();
        done=false;
    }
    
    
    
}
