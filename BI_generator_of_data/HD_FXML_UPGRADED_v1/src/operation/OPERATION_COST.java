/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operation;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author MychauU
 */
public enum OPERATION_COST {
    BUY_TICKET_SMALL_DISTANCE_MIN(50),
    BUY_TICKET_SMALL_DISTANCE_MAX(300),
    BUY_TICKET_LONG_DISTANCE_MIN(300),
    BUY_TICKET_LONG_DISTANCE_MAX(1000),
    DATE_CHANGE(50),
    PLACE_FIRST(0),
    PLACE_CHOOSE(20),
    HAND_BAGGAGE(0),
    BIGGER_HAND_BAGGAGE(70),
    REGISTERED_BAGGAGE(150),
    REGISTERED_BAGGAGE_CANCELED(0),
    BIGGER_HAND_BAGGAGE_CANCELED(0),
    CANCEL_TICKET(0);
    
    private final int operationCost;
    private static final List<OPERATION_COST> VALUES= Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static OPERATION_COST RANDOM_OPERATION_COST() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }

    private OPERATION_COST(int operationCost) {
        this.operationCost = operationCost;
    }

    public int GETVALUE() {
        return operationCost;
    }
}
