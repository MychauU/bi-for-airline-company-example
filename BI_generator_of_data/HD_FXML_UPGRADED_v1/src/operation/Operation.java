/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operation;

import java.util.Date;

/**
 *
 * @author MychauU
 */
public class Operation {

    public String nameOfUser;
    public String surnameOfUser;
    public String emailOfUser;
    public String typeOfService;
    public String nameOfOwner;
    public String surnameOfOwner;
    public String typeOfDocument;
    public String serialOfDocument;
    public String isAdult;
    public Date dateOfOperation;
    public Double Timezone;
    public int costOfOperation;
    public String additionalInformation;
    public String identifierCodeOfOwner;
    public long idCarriedFlight;
    public String numberOfTransaction;
    
    public Operation(String nameOfUser, String surnameOfUser, String emailOfUser, 
            String typeOfService, String nameOfOwner, String surnameOfOwner, 
            String typeOfDocument, String serialOfDocument, String isAdult, 
            Date dateOfOperation, Double Timezone, int costOfOperation, String additionalInformation, long idCarriedFlight,  String identifierCodeOfOwner,String numberOfTransaction) {
        this.nameOfUser = nameOfUser;
        this.surnameOfUser = surnameOfUser;
        this.emailOfUser = emailOfUser;
        this.typeOfService = typeOfService;
        this.nameOfOwner = nameOfOwner;
        this.surnameOfOwner = surnameOfOwner;
        this.typeOfDocument = typeOfDocument;
        this.serialOfDocument = serialOfDocument;
        this.isAdult = isAdult;
        this.dateOfOperation = dateOfOperation;
        this.Timezone = Timezone;
        this.costOfOperation = costOfOperation;
        this.additionalInformation = additionalInformation;
        this.idCarriedFlight = idCarriedFlight;
        this.identifierCodeOfOwner = identifierCodeOfOwner;
        this.numberOfTransaction=numberOfTransaction;
    }

}
