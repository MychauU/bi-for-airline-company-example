/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operation;

import utils.Serial;
import carried_flight.CarriedFlight;
import client.Names;
import client.Passager;
import client.People;
import client.Person;
import client.Surnames;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import utils.StaticFunctions;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class Operations {
    private static  Map<String,Integer> transactionCodes=new TreeMap<>();
    private static Serial serialTransactionCodes=new Serial(15);
    public long idOfCarriedFlightOperations;
    public List<Operation> operations;
    public boolean done;

    public Operations(long idOfCarriedFlightOperations) {
        this.idOfCarriedFlightOperations = idOfCarriedFlightOperations;
        operations = new ArrayList<>();
        done = false;
    }

    void createOperations(Map<String, Person> peoplePool, CarriedFlight carriedFlight, int limitPeople) {
        String serialPom;
        String serialBuy;
        int counter = 0;
        int id = 1;
        for (Person pomPerson : peoplePool.values()) {
            if (counter > limitPeople) {
                break;
            }
            int probability;
            Operation pomOperation;
            int operationCost;
            double pomTimezone;
            Date fromDate;
            Date toDate;
            Date baggageDate;
            Date changedPlaceDate;
            fromDate = new Date(carriedFlight.loadingDate.getTime());
            toDate = new Date(carriedFlight.flightStart.getTime());

            //time of buy ticket
            fromDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());

            pomTimezone = (double) StaticVariables.random.nextInt(24) - 12;

            if (carriedFlight.flightWsk.distance > 4000) {
                operationCost = StaticVariables.random.nextInt(OPERATION_COST.BUY_TICKET_LONG_DISTANCE_MAX.GETVALUE() - OPERATION_COST.BUY_TICKET_LONG_DISTANCE_MIN.GETVALUE()) + OPERATION_COST.BUY_TICKET_LONG_DISTANCE_MIN.GETVALUE();
            } else {
                operationCost = StaticVariables.random.nextInt(OPERATION_COST.BUY_TICKET_SMALL_DISTANCE_MAX.GETVALUE() - OPERATION_COST.BUY_TICKET_SMALL_DISTANCE_MIN.GETVALUE()) + OPERATION_COST.BUY_TICKET_SMALL_DISTANCE_MIN.GETVALUE();
            }

            //buy ticket
            
            serialPom=genTransactionCode();
            pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Kupno Biletu", pomPerson.name, pomPerson.surname,
                    pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", fromDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
            operations.add(pomOperation);
            counter++;
            if (pomPerson.dateRegistry.after(fromDate)) {
                pomPerson.dateRegistry = new Date(fromDate.getTime());
            }

            pomPerson.countBoughtTickets++;
            if (pomPerson.dateLastLogin.before(fromDate)) {
                pomPerson.dateLastLogin = new Date(fromDate.getTime());
            }
            //first place
            operationCost = OPERATION_COST.PLACE_FIRST.GETVALUE();
            pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Miejsce Początkowe", pomPerson.name, pomPerson.surname,
                    pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", fromDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
            operations.add(pomOperation);
            changedPlaceDate = new Date(fromDate.getTime());
            //first baggage
            operationCost = OPERATION_COST.HAND_BAGGAGE.GETVALUE();
            pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Bagaż podręczny", pomPerson.name, pomPerson.surname,
                    pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", fromDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
            operations.add(pomOperation);
            baggageDate = new Date(fromDate.getTime());

            //cancel
            probability = StaticVariables.random.nextInt(1000);
            if (probability < 5) {
                operationCost = OPERATION_COST.CANCEL_TICKET.GETVALUE();
                toDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                serialPom=genTransactionCode();
                pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Anulowanie biletu", pomPerson.name, pomPerson.surname,
                        pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", toDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                operations.add(pomOperation);
                if (pomPerson.dateLastLogin.before(toDate)) {
                    pomPerson.dateLastLogin = new Date(toDate.getTime());
                }
                carriedFlight.passagerList.add(new Passager(id++, fromDate, carriedFlight.id, pomPerson.name, pomPerson.surname, "Tak", pomPerson.typeOfDocument, pomPerson.serialNumber, "Odwołał lot"));
                counter--;

            } else {
                //date change
                probability = StaticVariables.random.nextInt(1000);
                if (probability < 10) {
                    operationCost = OPERATION_COST.DATE_CHANGE.GETVALUE();
                    serialPom=genTransactionCode();
                    toDate = new Date(StaticFunctions.nextLong(toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                    pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Zmiana daty biletu", pomPerson.name, pomPerson.surname,
                            pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", toDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                    operations.add(pomOperation);
                    if (pomPerson.dateLastLogin.before(toDate)) {
                        pomPerson.dateLastLogin = new Date(toDate.getTime());
                    }
                    carriedFlight.passagerList.add(new Passager(id++, fromDate, carriedFlight.id, pomPerson.name, pomPerson.surname, "Tak", pomPerson.typeOfDocument, pomPerson.serialNumber, "Przełożył lot"));
                    counter--;
                } else {
                    pomPerson.dateLastFlight = new Date(toDate.getTime());
                    probability = StaticVariables.random.nextInt(1000);
                    if (probability < 10) {
                        carriedFlight.passagerList.add(new Passager(id++, carriedFlight.flightStart, carriedFlight.id, pomPerson.name, pomPerson.surname, "Tak", pomPerson.typeOfDocument, pomPerson.serialNumber, "Nie stawił się na odprawie"));
                    } else {
                        carriedFlight.passagerList.add(new Passager(id++, fromDate, carriedFlight.id, pomPerson.name, pomPerson.surname, "Tak", pomPerson.typeOfDocument, pomPerson.serialNumber, "Wykonał lot"));
                    }

                }
            }

            probability = StaticVariables.random.nextInt(1000);
            //duzy bagaz podreczny
            if (probability < 200) {
                operationCost = OPERATION_COST.BIGGER_HAND_BAGGAGE.GETVALUE();
                baggageDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                serialPom=genTransactionCode();
                pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Duży bagaż podręczny", pomPerson.name, pomPerson.surname,
                        pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", baggageDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                operations.add(pomOperation);
                probability = StaticVariables.random.nextInt(1000);
                //anulowano bagaz duzy
                if (probability < 50) {
                    operationCost = OPERATION_COST.BIGGER_HAND_BAGGAGE_CANCELED.GETVALUE();
                    serialPom=genTransactionCode();
                    baggageDate = new Date(StaticFunctions.nextLong( toDate.getTime() - baggageDate.getTime() + 1) + baggageDate.getTime());
                    pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Anulowano duży bagaż podręczny", pomPerson.name, pomPerson.surname,
                            pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", baggageDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                    operations.add(pomOperation);
                }

            } else {
                // bagaz rejestrowany
                probability = StaticVariables.random.nextInt(1000);
                if (probability < 100) {
                    operationCost = OPERATION_COST.REGISTERED_BAGGAGE.GETVALUE();
                    serialPom=genTransactionCode();
                    baggageDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                    pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Bagaż rejestrowany", pomPerson.name, pomPerson.surname,
                            pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", baggageDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                    operations.add(pomOperation);
                    probability = StaticVariables.random.nextInt(1000);
                    //anulowano bagaz rejestrowany
                    if (probability < 10) {
                        operationCost = OPERATION_COST.REGISTERED_BAGGAGE_CANCELED.GETVALUE();
                        serialPom=genTransactionCode();
                        baggageDate = new Date(StaticFunctions.nextLong(toDate.getTime() - baggageDate.getTime() + 1) + baggageDate.getTime());
                        pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Anulowano bagaż rejestrowany", pomPerson.name, pomPerson.surname,
                                pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", baggageDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                        operations.add(pomOperation);
                    }
                }
            }
            if (pomPerson.dateLastLogin.before(baggageDate)) {
                pomPerson.dateLastLogin = new Date(baggageDate.getTime());
            }

            //changed place
            probability = StaticVariables.random.nextInt(1000);
            if (probability < 100) {
                operationCost = OPERATION_COST.PLACE_CHOOSE.GETVALUE();
                changedPlaceDate = new Date(StaticFunctions.nextLong(toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                serialPom=genTransactionCode();
                pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Zmiana miejsca", pomPerson.name, pomPerson.surname,
                        pomPerson.typeOfDocument, pomPerson.serialNumber, "Tak", changedPlaceDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                operations.add(pomOperation);
            }
            if (pomPerson.dateLastLogin.before(changedPlaceDate)) {
                pomPerson.dateLastLogin = new Date(changedPlaceDate.getTime());
            }
          /*  //scdp
            probability = StaticVariables.random.nextInt(1000);
            if (probability < 20) {
                pomPerson.changeDocument();
            }*/
        
            
            //handled people
            probability = StaticVariables.random.nextInt(1000);
            if (probability < 20) {
                if (counter > limitPeople) {
                    break;
                }
                if (pomPerson.passengers.isEmpty()) {
                    fromDate = new Date(carriedFlight.loadingDate.getTime());
                    toDate = new Date(carriedFlight.flightStart.getTime());
                    fromDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                    pomPerson.passengers.add(People.createPerson(fromDate, pomPerson.identifierCode));
                }
                probability = StaticVariables.random.nextInt(1000);
                if (probability < 100 && pomPerson.passengers.size() < 5) {
                    fromDate = new Date(carriedFlight.loadingDate.getTime());
                    toDate = new Date(carriedFlight.flightStart.getTime());
                    fromDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                    pomPerson.passengers.add(People.createPerson(fromDate, pomPerson.identifierCode));
                }
                
                int handledPeople = StaticVariables.random.nextInt(pomPerson.passengers.size());

                for (int i = 0; i < handledPeople; i++) {
                    if (counter > limitPeople) {
                        break;
                    }
                    Person handledPerson = pomPerson.passengers.get(StaticVariables.random.nextInt(pomPerson.passengers.size()));

                    if (carriedFlight.flightWsk.distance > 4000) {
                        operationCost = StaticVariables.random.nextInt(OPERATION_COST.BUY_TICKET_LONG_DISTANCE_MAX.GETVALUE() - OPERATION_COST.BUY_TICKET_LONG_DISTANCE_MIN.GETVALUE()) + OPERATION_COST.BUY_TICKET_LONG_DISTANCE_MIN.GETVALUE();
                    } else {
                        operationCost = StaticVariables.random.nextInt(OPERATION_COST.BUY_TICKET_SMALL_DISTANCE_MAX.GETVALUE() - OPERATION_COST.BUY_TICKET_SMALL_DISTANCE_MIN.GETVALUE()) + OPERATION_COST.BUY_TICKET_SMALL_DISTANCE_MIN.GETVALUE();
                    }

                    //buy ticket
                    serialPom=genTransactionCode();
                    probability = StaticVariables.random.nextInt(1000);
                    pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Kupno Biletu", handledPerson.name, handledPerson.surname,
                            handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, fromDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                    operations.add(pomOperation);
                    pomPerson.countBoughtTickets++;
                    if (pomPerson.dateLastLogin.before(fromDate)) {
                        pomPerson.dateLastLogin = new Date(fromDate.getTime());
                    }
                    counter++;
                    //first place
                    operationCost = OPERATION_COST.PLACE_FIRST.GETVALUE();
                    pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Miejsce Początkowe", handledPerson.name, handledPerson.surname,
                            handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, fromDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                    operations.add(pomOperation);
                    changedPlaceDate = new Date(fromDate.getTime());

                    //first baggage
                    operationCost = OPERATION_COST.HAND_BAGGAGE.GETVALUE();
                    pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Bagaż podręczny", handledPerson.name, handledPerson.surname,
                            handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, fromDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                    operations.add(pomOperation);
                    baggageDate = new Date(fromDate.getTime());

                    //cancel
                    probability = StaticVariables.random.nextInt(1000);
                    if (probability < 5) {
                        operationCost = OPERATION_COST.CANCEL_TICKET.GETVALUE();
                        serialPom=genTransactionCode();
                        toDate = new Date(StaticFunctions.nextLong(toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                        pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Anulowanie biletu", handledPerson.name, handledPerson.surname,
                                handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, toDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                        operations.add(pomOperation);
                        if (pomPerson.dateLastLogin.before(toDate)) {
                            pomPerson.dateLastLogin = new Date(toDate.getTime());
                        }
                        carriedFlight.passagerList.add(new Passager(id++, carriedFlight.flightStart, carriedFlight.id, handledPerson.name, handledPerson.surname, handledPerson.isAdult, handledPerson.typeOfDocument, handledPerson.serialNumber, "Odwołał lot"));
                        counter--;
                    } else {
                        //date change
                        probability = StaticVariables.random.nextInt(1000);
                        if (probability < 10) {
                            operationCost = OPERATION_COST.DATE_CHANGE.GETVALUE();
                            toDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                            pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Zmiana daty biletu", handledPerson.name, handledPerson.surname,
                                    handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, toDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                            operations.add(pomOperation);
                            if (pomPerson.dateLastLogin.before(toDate)) {
                                pomPerson.dateLastLogin = new Date(toDate.getTime());
                            }
                            carriedFlight.passagerList.add(new Passager(id++, carriedFlight.flightStart, carriedFlight.id, handledPerson.name, handledPerson.surname, handledPerson.isAdult, handledPerson.typeOfDocument, handledPerson.serialNumber, "Przełożył lot"));
                            counter--;
                        } else {
                            probability = StaticVariables.random.nextInt(1000);
                            if (probability < 10) {
                                carriedFlight.passagerList.add(new Passager(id++, carriedFlight.flightStart, carriedFlight.id, handledPerson.name, handledPerson.surname, handledPerson.isAdult, handledPerson.typeOfDocument, handledPerson.serialNumber, "Nie stawił się na odprawie"));
                            } else {
                                carriedFlight.passagerList.add(new Passager(id++, carriedFlight.flightStart, carriedFlight.id, handledPerson.name, handledPerson.surname, handledPerson.isAdult, handledPerson.typeOfDocument, handledPerson.serialNumber, "Wykonał lot"));
                            }
                        }
                    }

                    probability = StaticVariables.random.nextInt(1000);
                    //duzy bagaz podreczny
                    if (probability < 200) {
                        serialPom=genTransactionCode();
                        operationCost = OPERATION_COST.BIGGER_HAND_BAGGAGE.GETVALUE();
                        baggageDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                        pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Duży bagaż podręczny", handledPerson.name, handledPerson.surname,
                                handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, baggageDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                        operations.add(pomOperation);
                        probability = StaticVariables.random.nextInt(1000);
                        //anulowano bagaz duzy
                        if (probability < 50) {
                            serialPom=genTransactionCode();
                            operationCost = OPERATION_COST.BIGGER_HAND_BAGGAGE_CANCELED.GETVALUE();
                            baggageDate = new Date(StaticFunctions.nextLong( toDate.getTime() - baggageDate.getTime() + 1) + baggageDate.getTime());
                            pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Anulowano duży bagaż podręczny", handledPerson.name, handledPerson.surname,
                                    handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, baggageDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                            operations.add(pomOperation);
                        }

                    } else {
                        // bagaz rejestrowany
                        probability = StaticVariables.random.nextInt(1000);
                        if (probability < 100) {
                            operationCost = OPERATION_COST.REGISTERED_BAGGAGE.GETVALUE();
                            serialPom=genTransactionCode();
                            baggageDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                            pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Bagaż rejestrowany", handledPerson.name, handledPerson.surname,
                                    handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, baggageDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                            operations.add(pomOperation);
                            probability = StaticVariables.random.nextInt(1000);
                            //anulowano bagaz rejestrowany
                            if (probability < 10) {
                                operationCost = OPERATION_COST.REGISTERED_BAGGAGE_CANCELED.GETVALUE();
                                serialPom=genTransactionCode();
                                baggageDate = new Date(StaticFunctions.nextLong( toDate.getTime() - baggageDate.getTime() + 1) + baggageDate.getTime());
                                pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Anulowano bagaż rejestrowany", handledPerson.name, handledPerson.surname,
                                        handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, baggageDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                                operations.add(pomOperation);
                            }
                        }
                    }
                    if (pomPerson.dateLastLogin.before(baggageDate)) {
                        pomPerson.dateLastLogin = new Date(baggageDate.getTime());
                    }

                    //changed place
                    probability = StaticVariables.random.nextInt(1000);
                    if (probability < 100) {
                        operationCost = OPERATION_COST.PLACE_CHOOSE.GETVALUE();
                        serialPom=genTransactionCode();
                        changedPlaceDate = new Date(StaticFunctions.nextLong( toDate.getTime() - fromDate.getTime() + 1) + fromDate.getTime());
                        pomOperation = new Operation(pomPerson.name, pomPerson.surname, pomPerson.email, "Zmiana miejsca", handledPerson.name, handledPerson.surname,
                                handledPerson.typeOfDocument, handledPerson.serialNumber, handledPerson.isAdult, changedPlaceDate, pomTimezone, operationCost, "0", carriedFlight.id, pomPerson.identifierCode,serialPom);
                        operations.add(pomOperation);
                    }
                    if (pomPerson.dateLastLogin.before(changedPlaceDate)) {
                        pomPerson.dateLastLogin = new Date(changedPlaceDate.getTime());
                    }
                 /*   //scdp
                    probability = StaticVariables.random.nextInt(1000);
                    if (probability < 20) {
                        handledPerson.changeDocument();
                    }
*/
                }

            }
        }

    }
    private static String genTransactionCode(){
        String pom;
        do {
            pom=serialTransactionCodes.nextSerial();
        }while(transactionCodes.containsKey(pom));
        transactionCodes.put(pom, 1);
        return pom;
    }

}
