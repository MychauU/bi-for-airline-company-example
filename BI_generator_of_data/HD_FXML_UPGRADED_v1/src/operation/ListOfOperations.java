/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operation;

import static airplane.Airplanes.MAX_CAPACITY;
import carried_flight.CarriedFlights;
import client.People;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import airplane_executing.*;
import carried_flight.CarriedFlight;
import client.Person;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class ListOfOperations {

    public Map<Long, Operations> listOfOperations;
    CarriedFlights carriedFlightsPool;
    People peoplePool;
    AirplanesExecuting AirplanesExecutingPool;
    String[] keyPeoplePool;

    public ListOfOperations(CarriedFlights carriedFlightsPool, People peoplePool, AirplanesExecuting AirplanesExecutingPool) {
        listOfOperations = new TreeMap<>();
        this.carriedFlightsPool = carriedFlightsPool;
        this.peoplePool = peoplePool;
        this.AirplanesExecutingPool = AirplanesExecutingPool;
    }

    public void createListOfOperations() {
        keyPeoplePool = peoplePool.people.keySet().toArray(new String[peoplePool.people.size()]);
        int maxCapacity80 = (int) Math.floor((((double) MAX_CAPACITY) * 0.8));
        Set<Long> keySetPool = carriedFlightsPool.carriedFlights.keySet();

        /*Map<Long, AirplaneExecuting> airPlaneExecutingFilterResult;
        for (long key : keySetPool) {
            airPlaneExecutingFilterResult = AirplanesExecutingPool.airplanesExecuting.entrySet().stream()
                    .filter(predicate -> key == predicate.getValue().carriedFlightId)
                    .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
            List<AirplaneExecuting> airplaneList = new ArrayList<>(airPlaneExecutingFilterResult.values());
            AirplaneExecuting airplaneResult=airplaneList.get(0);
            
        }*/
        
        Operations pomOperations;
        for (CarriedFlight value : carriedFlightsPool.carriedFlights.values()) {

            boolean carriedFlightsExecutedOperations = this.listOfOperations.entrySet().stream()
                    .anyMatch(predicate -> value.id == predicate.getValue().idOfCarriedFlightOperations);
            if (carriedFlightsExecutedOperations) {

            } else {
                Map<String, Person> pomPeople = new TreeMap<>();

                int capacity = StaticVariables.random.nextInt(MAX_CAPACITY - maxCapacity80) + maxCapacity80;
                for (int i = 0; i < capacity;) {
                    Person pomPerson = peoplePool.people.get(keyPeoplePool[StaticVariables.random.nextInt(keyPeoplePool.length)]);

                    if (pomPeople.get(pomPerson.email) == null) {
                        pomPeople.put(pomPerson.email, pomPerson);
                        i++;
                    }

                }

                pomOperations = new Operations(value.id);
                listOfOperations.put(value.id, pomOperations);
                pomOperations.createOperations(pomPeople, value, capacity);
                pomPeople.clear();
            }
        }
    }
    
    
}
