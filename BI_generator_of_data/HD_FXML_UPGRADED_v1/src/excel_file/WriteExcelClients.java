/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excel_file;

import client.Person;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author Arkadiusz
 */
public class WriteExcelClients {

    Map<String, Person> people;

    public WriteExcelClients(Map<String, Person> people) {
        this.people = people;

    }

    public void writeToExcel(String fileName) {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet(String.valueOf(0));

        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        int rownum = 0;
        Row row = sheet.createRow(rownum++);
        int cellnum = 0;

        Set<String> keyset = people.keySet();
        Cell cell = row.createCell(cellnum++);
        cell.setCellValue("imie_uzytkownika");
        cell = row.createCell(cellnum++);
        cell.setCellValue("nazwisko_uzytkownika");
        cell = row.createCell(cellnum++);
        cell.setCellValue("kod_identyfikujacy");
        cell = row.createCell(cellnum++);
        cell.setCellValue("email_uzytkownika");
        cell = row.createCell(cellnum++);
        cell.setCellValue("narodowosc");
        cell = row.createCell(cellnum++);
        cell.setCellValue("data_zarejestrowania");
        cell = row.createCell(cellnum++);
        cell.setCellValue("data_odbytego_ostatniego_lotu");
        cell = row.createCell(cellnum++);
        cell.setCellValue("liczba_zakupionych_biletow");
        cell = row.createCell(cellnum++);
        cell.setCellValue("data_ostatniego_zalogowania");
        
        cell = row.createCell(cellnum++);
        cell.setCellValue("typ_dokumentu");
        cell = row.createCell(cellnum++);
        cell.setCellValue("numer_dokumentu");
        cell = row.createCell(cellnum++);
        cell.setCellValue("kod_identyfikujacy_klienta");
        
        for (String key : keyset) {
            row = sheet.createRow(rownum++);
            Person person = people.get(key);
            cellnum = 0;

            cell = row.createCell(cellnum++);
            cell.setCellValue(person.name);
            cell = row.createCell(cellnum++);
            cell.setCellValue(person.surname);
            cell = row.createCell(cellnum++);
            cell.setCellValue(person.identifierCode);
            cell = row.createCell(cellnum++);
            cell.setCellValue(person.email);
            cell = row.createCell(cellnum++);
            cell.setCellValue(person.country);
            cell = row.createCell(cellnum++);
            cell.setCellValue(ft.format(person.dateRegistry));
            cell = row.createCell(cellnum++);
            cell.setCellValue(ft.format(person.dateLastFlight));
            cell = row.createCell(cellnum++);
            cell.setCellValue(person.countBoughtTickets);
            cell = row.createCell(cellnum++);
            cell.setCellValue(ft.format(person.dateLastLogin));
            
            cell = row.createCell(cellnum++);
            cell.setCellValue(person.typeOfDocument);
            cell = row.createCell(cellnum++);
            cell.setCellValue(person.serialNumber);
            cell = row.createCell(cellnum++);
            cell.setCellValue(person.clientIdentifierCode);
            
            for (Person pass: person.passengers){
                row = sheet.createRow(rownum++);
                cellnum = 0;

                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.name);
                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.surname);
                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.identifierCode);
                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.email);
                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.country);
                cell = row.createCell(cellnum++);
                cell.setCellValue(ft.format(pass.dateRegistry));
                cell = row.createCell(cellnum++);
                cell.setCellValue(ft.format(pass.dateLastFlight));
                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.countBoughtTickets);
                cell = row.createCell(cellnum++);
                cell.setCellValue(ft.format(pass.dateLastLogin));

                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.typeOfDocument);
                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.serialNumber);
                cell = row.createCell(cellnum++);
                cell.setCellValue(pass.clientIdentifierCode);
            }
            
        }

        try {
            FileOutputStream out = new FileOutputStream(new File("output\\" + fileName));
            workbook.write(out);
            out.close();
            System.out.println("Excel written successfully..");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
