/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excel_file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Set;
import operation.ListOfOperations;
import operation.Operation;
import operation.Operations;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author Arkadiusz
 */
public class WriteExcelOperations {

    ListOfOperations listOfOperations;

    public WriteExcelOperations(ListOfOperations listOfOperations) {
        this.listOfOperations = listOfOperations;
    }

    public void writeToExcel(String fileOperationName) {

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = null;
        int nextFile=1;
        
        int counter = 0;
        int rownum = 0;
        int cellnum = 0;
        Row row;
        Cell cell;

        Set<Long> keyset = listOfOperations.listOfOperations.keySet();
        boolean newFile = true;
        for (Long key : keyset) {
            Operations operations = listOfOperations.listOfOperations.get(key);

            if (newFile) {
                workbook = new HSSFWorkbook();
                sheet = workbook.createSheet("0");
                rownum = 0;
                cellnum = 0;
                row = sheet.createRow(rownum++);

                cell = row.createCell(cellnum++);
                cell.setCellValue("id_realizacji_lotu");
                cell = row.createCell(cellnum++);
                cell.setCellValue("imie_uzytkownika");
                cell = row.createCell(cellnum++);
                cell.setCellValue("nazwisko_uzytkownika");
                cell = row.createCell(cellnum++);
                cell.setCellValue("email_uzytkownika");
                cell = row.createCell(cellnum++);
                cell.setCellValue("typ_uslugi");
                cell = row.createCell(cellnum++);
                cell.setCellValue("imie_wlasciciela_biletu");
                cell = row.createCell(cellnum++);
                cell.setCellValue("nazwisko_wlasciciela_biletu");
                cell = row.createCell(cellnum++);
                cell.setCellValue("kod_identyfikujacy_wlasciciela_biletu");
                cell = row.createCell(cellnum++);
                cell.setCellValue("typ_dokumentu_wlasiciela_biletu");
                cell = row.createCell(cellnum++);
                cell.setCellValue("numer_dokumentu_wlasciciela_biletu");
                cell = row.createCell(cellnum++);
                cell.setCellValue("czy_pelnoletni_wlasciciel_biletu");
                cell = row.createCell(cellnum++);
                cell.setCellValue("data_operacji");
                cell = row.createCell(cellnum++);
                cell.setCellValue("czas_operacji");
                cell = row.createCell(cellnum++);
                cell.setCellValue("strefa_czasowa_operacji");
                cell = row.createCell(cellnum++);
                cell.setCellValue("cena_operacji");
                cell = row.createCell(cellnum++);
                cell.setCellValue("informacje_dodatkowe");
                cell = row.createCell(cellnum++);
                cell.setCellValue("numer_transakcji");
                newFile = false;
            }
            
            for (Operation operation : operations.operations) {
                counter++;
                row = sheet.createRow(rownum++);
                cellnum = 0;
                cell = row.createCell(cellnum++);
                cell.setCellValue(operations.idOfCarriedFlightOperations);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.nameOfUser);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.surnameOfUser);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.emailOfUser);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.typeOfService);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.nameOfOwner);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.surnameOfOwner);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.identifierCodeOfOwner);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.typeOfDocument);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.serialOfDocument);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.isAdult);
                cell = row.createCell(cellnum++);

                String date = new SimpleDateFormat("yyyy-MM-dd").format(operation.dateOfOperation);

                cell.setCellValue(date);
                cell = row.createCell(cellnum++);

                SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
                String time = localDateFormat.format(operation.dateOfOperation);

                cell.setCellValue(time);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.Timezone);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.costOfOperation);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.additionalInformation);
                cell = row.createCell(cellnum++);
                cell.setCellValue(operation.numberOfTransaction);
            }
            if (counter > 50000) {

                try {
                    FileOutputStream out = new FileOutputStream(new File("output\\" + fileOperationName+"_"+nextFile+ ".xls"));
                    nextFile++;
                    newFile=true;
                    workbook.write(out);
                    out.close();
                    System.out.println("Excel number written successfully..");
                    workbook.close();
                    counter = 0;

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        if (counter != 0) {
            try {
                FileOutputStream out = new FileOutputStream(new File("output\\" + fileOperationName + ".xls"));
                workbook.write(out);
                out.close();
                System.out.println("Excel number written successfully..");
                workbook.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
