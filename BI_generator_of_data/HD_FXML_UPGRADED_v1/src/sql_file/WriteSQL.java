/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sql_file;

import airplane.Airplane;
import airplane.Airplanes;
import airplane_executing.AirplaneExecuting;
import airplane_executing.AirplanesExecuting;
import airport.Airport;
import airport.Airports;
import carried_flight.CarriedFlight;
import carried_flight.CarriedFlights;
import client.Passager;
import flight.Flight;
import flight.Flights;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MychauU
 */
public class WriteSQL {

    String delimer;
    String delfont;

    public WriteSQL() {
        delimer = ",";
        delfont = "\'";
    }

    public void createSQLFile(String fileName) {
        try {
            PrintWriter writer = new PrintWriter("output\\" + fileName, "UTF-8");
            writer.println("DROP TABLE Pasazer\n"
                    + "DROP TABLE Samolot_Realizujacy\n"
                    + "DROP TABLE Realizacja_lotu\n"
                    + "DROP TABLE Lot\n"
                    + "DROP TABLE Port_lotniczy\n"
                    + "DROP TABLE Samolot\n"
                    + "\n"
                    + "CREATE TABLE Port_lotniczy\n"
                    + "(\n"
                    + "	ID_portu integer PRIMARY KEY,\n"
                    + "	Nazwa varchar(80) NOT NULL,\n"
                    + "	Miejscowosc varchar(40) NOT NULL,\n"
                    + "	Kraj varchar(40) NOT NULL,\n"
                    + "	Strefa_czasowa float NOT NULL\n"
                    + ");\n"
                    + "\n"
                    + "CREATE TABLE Lot\n"
                    + "(\n"
                    + "	ID_lotu integer PRIMARY KEY,\n"
                    + "	Odleglosc integer NOT NULL,\n"
                    + "	ID_port_skad integer NOT NULL REFERENCES Port_lotniczy(ID_portu),\n"
                    + "	ID_port_dokad integer NOT NULL REFERENCES Port_lotniczy(ID_portu)\n"
                    + ");\n"
                    + "\n"
                    + "CREATE TABLE Samolot\n"
                    + "(\n"
                    + "	ID_samolotu integer PRIMARY KEY,\n"
                    + "	Model varchar(20) NOT NULL,\n"
                    + "	Max_miejsc integer NOT NULL,\n"
                    + "	Nazwa_seryjna varchar(20) UNIQUE NOT NULL,\n"
                    + "	Waga integer NOT NULL,\n"
                    + "	Szerokosc integer NOT NULL\n"
                    + ");\n"
                    + "\n"
                    + "\n"
                    + "CREATE TABLE Realizacja_lotu\n"
                    + "(\n"
                    + "	ID_wyk_lotu integer PRIMARY KEY,\n"
                    + "	Data_dodania smalldatetime NOT NULL,\n"
                    + "	ID_lotu integer NOT NULL REFERENCES Lot(ID_lotu),\n"
                    + "	Data_planowanego_startu smalldatetime NOT NULL,\n"
                    + "	Data_planowanego_ladowania smalldatetime NOT NULL,\n"
                    + "	Data_startu smalldatetime NOT NULL,\n"
                    + "	Data_ladowania smalldatetime NOT NULL,\n"
                    + "	Status varchar(20) NOT NULL\n"
                    + "	\n"
                    + ");\n"
                    + "\n"
                    + "CREATE TABLE Samolot_Realizujacy\n"
                    + "(\n"
                    + "	ID_samolotu_realizujacego integer PRIMARY KEY,\n"
                    + "	ID_samolotu integer NOT NULL REFERENCES Samolot(ID_samolotu),\n"
                    + "	Data_nadania_samolotu smalldatetime NOT NULL,\n"
                    + "	Status varchar(20) NOT NULL\n"
                    + ");\n"
                    + "\n"
                    + "CREATE TABLE Pasazer\n"
                    + "(\n"
                    + "	ID_osoby integer PRIMARY KEY IDENTITY(1,1),\n"
                    + "	Data_dolaczenia smalldatetime NOT NULL,\n"
                    + "	ID_realizacji_lotu integer NOT NULL REFERENCES Realizacja_lotu(ID_wyk_lotu),\n"
                    + "	Imie varchar(20) NOT NULL,\n"
                    + "	Nazwisko varchar(20) NOT NULL,\n"
                    + "	Pelnoletnosc varchar(5) NOT NULL,\n"
                    + "	Typ_dokumentu varchar(20) NOT NULL,\n"
                    + "	Numer_dokumentu varchar(20) NOT NULL,\n"
                    + "	Status varchar(40) NOT NULL\n"
                    + ");\n"
                    + "\n"
                    + "\n"
                    + "");
            writer.close();
        } catch (Exception e) {
            // do something
        }
    }

    public void writeToSQLFileAirports(String fileName, Airports airportsPool) {
        int counter = 1;
        StringBuilder pomString = new StringBuilder();
        try (FileWriter fw = new FileWriter("output\\" + fileName, false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter writer = new PrintWriter(bw)) {

            writer.println("insert into dbo.Port_lotniczy ( ID_portu, Nazwa, Miejscowosc, Kraj, Strefa_czasowa) values \n");

            for (Airport pom : airportsPool.airports) {

                if (counter % 999 == 0) {
                    pomString.deleteCharAt(pomString.length() - 1);
                    pomString.append(";\n");
                    writer.println(pomString.toString());
                    pomString = new StringBuilder();
                    counter = 1;
                    writer.println("insert into dbo.Port_lotniczy ( ID_portu, Nazwa, Miejscowosc, Kraj, Strefa_czasowa) values \n");
                }

                pomString.append("\n(");

                pomString.append(pom.getId()).append(delimer);
                pomString.append(delfont).append(pom.name).append(delfont).append(delimer);
                pomString.append(delfont).append(pom.city).append(delfont).append(delimer);
                pomString.append(delfont).append(pom.country).append(delfont).append(delimer);
                pomString.append(pom.timezone);

                pomString.append("),");
                counter++;
            }
            pomString.deleteCharAt(pomString.length() - 1);
            pomString.append(";\n");
            writer.println(pomString.toString());

            writer.close();
            bw.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public void writeToFile(String fileName, Airports airportsPool) {
        try (FileWriter fw = new FileWriter("output\\" + fileName, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter writer = new PrintWriter(bw)) {
            StringBuilder pomString = new StringBuilder();
            for (Airport pom : airportsPool.airports) {

                pomString.append(pom.getId()).append(delimer);
                pomString.append(pom.name).append(delimer);
                pomString.append(pom.city).append(delimer);
                pomString.append(pom.country).append(delimer);
                pomString.append(pom.timezone);

                pomString.append("),");
            }
            pomString.deleteCharAt(pomString.length() - 1);
            pomString.append(";");

            writer.println(pomString.toString());

            writer.close();
            bw.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public void writeToSQLFileAirplanes(String fileName, Airplanes airplanesPool) {
        int counter = 1;
        StringBuilder pomString = new StringBuilder();
        try (FileWriter fw = new FileWriter("output\\" + fileName, false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter writer = new PrintWriter(bw)) {

            writer.println("insert into dbo.Samolot ( ID_samolotu, Model, Max_miejsc, Nazwa_seryjna, Waga, Szerokosc) values \n");

            for (Airplane pom : airplanesPool.ariplanes.values()) {

                if (pom.done == false) {
                    pom.done = true;

                    if (counter % 999 == 0) {
                        pomString.deleteCharAt(pomString.length() - 1);
                        pomString.append(";\n");
                        writer.println(pomString.toString());
                        pomString = new StringBuilder();
                        counter = 1;
                        writer.println("insert into dbo.Samolot ( ID_samolotu, Model, Max_miejsc, Nazwa_seryjna, Waga, Szerokosc) values \n");
                    }

                    pomString.append("\n(");

                    pomString.append(pom.id).append(delimer);
                    pomString.append(delfont).append(pom.model).append(delfont).append(delimer);
                    pomString.append(pom.capacity).append(delimer);
                    pomString.append(delfont).append(pom.serial).append(delfont).append(delimer);
                    pomString.append(pom.capacity).append(delimer);
                    pomString.append(pom.capacity);
                    pomString.append("),");
                    counter++;
                }
            }
            pomString.deleteCharAt(pomString.length() - 1);
            pomString.append(";\n");
            writer.println(pomString.toString());

            writer.close();
            bw.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public void writeToSQLFileFlights(String fileName, Flights flightsPool) {
        int counter = 1;
        StringBuilder pomString = new StringBuilder();
        try (FileWriter fw = new FileWriter("output\\" + fileName, false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter writer = new PrintWriter(bw)) {

            writer.println("insert into dbo.Lot ( ID_lotu, Odleglosc, ID_port_skad, ID_port_dokad) values \n");

            for (Flight pom : flightsPool.flights.values()) {
                if (pom.done == false) {
                    pom.done = true;
                    if (counter % 999 == 0) {
                        pomString.deleteCharAt(pomString.length() - 1);
                        pomString.append(";\n");
                        writer.println(pomString.toString());
                        pomString = new StringBuilder();
                        counter = 1;
                        writer.println("insert into dbo.Lot ( ID_lotu, Odleglosc, ID_port_skad, ID_port_dokad) values \n");
                    }

                    pomString.append("\n(");

                    pomString.append(pom.id).append(delimer);
                    pomString.append(pom.distance).append(delimer);
                    pomString.append(pom.from.id).append(delimer);
                    pomString.append(pom.to.id);
                    pomString.append("),");
                    counter++;
                }
            }
            pomString.deleteCharAt(pomString.length() - 1);
            pomString.append(";\n");
            writer.println(pomString.toString());

            writer.close();
            bw.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public void writeToSQLFileCarriedFlights(String fileName, CarriedFlights carriedFlightsPool) {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        int counter = 1;
        StringBuilder pomString = new StringBuilder();
        try (FileWriter fw = new FileWriter("output\\" + fileName, false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter writer = new PrintWriter(bw)) {
            writer.println("insert into dbo.Realizacja_lotu ( ID_wyk_lotu, Data_dodania, ID_lotu, Data_planowanego_startu, Data_planowanego_ladowania, Data_startu, Data_ladowania, Status) values \n");

            for (CarriedFlight pom : carriedFlightsPool.carriedFlights.values()) {
                if (pom.done == false) {
                    pom.done = true;
                    if (counter % 999 == 0) {
                        pomString.deleteCharAt(pomString.length() - 1);
                        pomString.append(";\n");
                        writer.println(pomString.toString());
                        pomString = new StringBuilder();
                        counter = 1;
                        writer.println("insert into dbo.Realizacja_lotu ( ID_wyk_lotu, Data_dodania, ID_lotu, Data_planowanego_startu, Data_planowanego_ladowania, Data_startu, Data_ladowania, Status) values \n");
                    }

                    pomString.append("\n(");

                    pomString.append(pom.id).append(delimer);
                    pomString.append(delfont).append(ft.format(pom.loadingDate)).append(delfont).append(delimer);
                    pomString.append(pom.flightId).append(delimer);
                    pomString.append(delfont).append(ft.format(pom.scheduledFlightStart)).append(delfont).append(delimer);
                    pomString.append(delfont).append(ft.format(pom.scheduledFlightEnd)).append(delfont).append(delimer);
                    pomString.append(delfont).append(ft.format(pom.flightStart)).append(delfont).append(delimer);
                    pomString.append(delfont).append(ft.format(pom.flightEnd)).append(delfont).append(delimer);
                    pomString.append(delfont).append(pom.status).append(delfont);
                    pomString.append("),");
                    counter++;
                }
            }
            pomString.deleteCharAt(pomString.length() - 1);
            pomString.append(";\n");
            writer.println(pomString.toString());

            writer.close();
            bw.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public void writeToSQLFileAirplanesExecuting(String fileName, AirplanesExecuting airplanesExecutingPool) {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        int counter = 1;
        StringBuilder pomString = new StringBuilder();
        try (FileWriter fw = new FileWriter("output\\" + fileName, false);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter writer = new PrintWriter(bw)) {

            writer.println("insert into dbo.Samolot_realizujacy ( ID_samolotu_realizujacego, ID_samolotu, Data_nadania_samolotu, Status) values \n");

            for (AirplaneExecuting pom : airplanesExecutingPool.airplanesExecuting.values()) {
                if (pom.done == false) {
                    pom.done = true;
                    if (counter % 999 == 0) {
                        pomString.deleteCharAt(pomString.length() - 1);
                        pomString.append(";\n");
                        writer.println(pomString.toString());
                        pomString = new StringBuilder();
                        counter = 1;
                        writer.println("insert into dbo.Samolot_realizujacy ( ID_samolotu_realizujacego, ID_samolotu, Data_nadania_samolotu, Status) values \n");
                    }

                    pomString.append("\n(");

                    pomString.append(pom.id).append(delimer);
                    pomString.append(pom.airplaneId).append(delimer);
                    pomString.append(delfont).append(ft.format(pom.dateOfPosting)).append(delfont).append(delimer);
                    pomString.append(delfont).append(pom.status).append(delfont);
                    pomString.append("),");
                    counter++;
                }
            }
            pomString.deleteCharAt(pomString.length() - 1);
            pomString.append(";\n");
            writer.println(pomString.toString());

            writer.close();
            bw.close();
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public void writeToSQLFilePassengers(String fileName, CarriedFlights carriedFlightsPool) {
        try {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            int counterOfAll = 1;
            int counterOfFiles = 1;
            FileWriter fw;
            BufferedWriter bw;
            PrintWriter writer;
            int counter = 1;
            StringBuilder pomString = new StringBuilder();
            fw = new FileWriter("output\\" + fileName + counterOfFiles + ".sql", false);
            bw = new BufferedWriter(fw);
            writer = new PrintWriter(bw);

            writer.println("insert into dbo.Pasazer (  Data_dolaczenia, ID_realizacji_lotu, Imie,Nazwisko,Pelnoletnosc,Typ_dokumentu,Numer_dokumentu,Status) values \n");

            for (CarriedFlight pom : carriedFlightsPool.carriedFlights.values()) {
                if (pom.done == false) {
                    for (Passager pomPassenger : pom.passagerList) {

                        if (counter % 999 == 0) {
                            pomString.deleteCharAt(pomString.length() - 1);
                            pomString.append(";\n");
                            writer.println(pomString.toString());
                            pomString = new StringBuilder();
                            counter = 1;
                            writer.println("insert into dbo.Pasazer ( Data_dolaczenia, ID_realizacji_lotu, Imie,Nazwisko,Pelnoletnosc,Typ_dokumentu,Numer_dokumentu,Status) values \n");
                        }
                        if (counterOfAll % 5000 == 0) {
                            counterOfAll = 1;
                            counterOfFiles++;
                            pomString.deleteCharAt(pomString.length() - 1);
                            pomString.append(";\n");
                            writer.println(pomString.toString());

                            writer.close();
                            bw.close();
                            fw.close();
                            System.out.println("Sql_passenger number " + (counterOfFiles - 1) + " written successfully..");
                            fw = new FileWriter("output\\" + fileName + counterOfFiles + ".sql", false);
                            bw = new BufferedWriter(fw);
                            writer = new PrintWriter(bw);
                            pomString = new StringBuilder();
                            counter = 1;
                            writer.println("insert into dbo.Pasazer ( Data_dolaczenia, ID_realizacji_lotu, Imie,Nazwisko,Pelnoletnosc,Typ_dokumentu,Numer_dokumentu,Status) values \n");
                        }

                        pomString.append("\n(");

                        pomString.append(delfont).append(ft.format(pomPassenger.dateAdded)).append(delfont).append(delimer);
                        pomString.append(pomPassenger.idCarriedFlight).append(delimer);
                        pomString.append(delfont).append(pomPassenger.name).append(delfont).append(delimer);
                        pomString.append(delfont).append(pomPassenger.surname).append(delfont).append(delimer);
                        pomString.append(delfont).append(pomPassenger.isAdult).append(delfont).append(delimer);
                        pomString.append(delfont).append(pomPassenger.typeOfDocument).append(delfont).append(delimer);
                        pomString.append(delfont).append(pomPassenger.serialNumber).append(delfont).append(delimer);
                        pomString.append(delfont).append(pomPassenger.status).append(delfont);
                        pomString.append("),");
                        counter++;
                        counterOfAll++;
                    }
                }
            }
            pomString.deleteCharAt(pomString.length() - 1);
            pomString.append(";\n");
            writer.println(pomString.toString());

            writer.close();
            bw.close();
            fw.close();

        } catch (IOException ex) {
            Logger.getLogger(WriteSQL.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

}
