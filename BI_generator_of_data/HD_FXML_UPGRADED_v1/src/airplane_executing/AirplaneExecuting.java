/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airplane_executing;

import java.util.Date;

/**
 *
 * @author MychauU
 */
public class AirplaneExecuting {

    public long id;
    public long airplaneId;
    public long carriedFlightId;
    public Date dateOfPosting;
    public String status;
    public boolean done;
    public AirplaneExecuting(long id, long airplaneId,long carriedFlightId, Date dateOfPosting, String status) {
        this.id=id;
        this.airplaneId=airplaneId;
        this.dateOfPosting=dateOfPosting;
        this.status=status;
        this.carriedFlightId=carriedFlightId;
        done=false;
    }

}
