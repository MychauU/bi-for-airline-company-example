/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airplane_executing;

import airplane.Airplane;
import airplane.Airplanes;
import carried_flight.CarriedFlight;
import carried_flight.CarriedFlights;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.stream.Collectors;
import utils.StaticFunctions;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class AirplanesExecuting {

    Airplanes airplanes;
    CarriedFlights carriedFlights;
    public Map<Long, AirplaneExecuting> airplanesExecuting;
    long counter;

    public AirplanesExecuting(Airplanes airplanes, CarriedFlights carriedFlights) {
        this.airplanes = airplanes;
        this.carriedFlights = carriedFlights;
        airplanesExecuting = new TreeMap<>();
        counter = 1;
    }

    public void createAirplanesExecuting() {
        AirplaneExecuting result;
        List<String> poms = new ArrayList<String>(airplanes.ariplanes.keySet());

        for (long i = 0; i < carriedFlights.carriedFlights.size();) {
            CarriedFlight pomCarriedFlight = carriedFlights.carriedFlights.get(i + 1);
            
            boolean carriedFlightExecutedByAirplane = this.airplanesExecuting.entrySet().stream()
                    .anyMatch(predicate -> pomCarriedFlight.id == predicate.getValue().carriedFlightId);
            if (carriedFlightExecutedByAirplane) {
                i++;
            } else {
                Airplane pomAirplane = airplanes.ariplanes.get(poms.get(StaticVariables.random.nextInt(poms.size())));
                Airplane pomAirplane2 = null;
                Airplane pomAirplane3 = null;
                //zmieniana data
                Date pomDate = pomCarriedFlight.loadingDate;

                String pomStatus = "Pierwsze nadanie";

                //pierwsze nadanie
                Date pomloadingDate = pomCarriedFlight.loadingDate;
                Calendar pomloadingDateCalendar = Calendar.getInstance();
                pomloadingDateCalendar.setTime(pomloadingDate);
                pomloadingDateCalendar.add(Calendar.MINUTE, StaticVariables.random.nextInt(60));
                pomloadingDateCalendar.add(Calendar.HOUR_OF_DAY, StaticVariables.random.nextInt(24));
                pomDate.setTime(pomloadingDateCalendar.getTimeInMillis());
                result = new AirplaneExecuting(counter, pomAirplane.id, pomCarriedFlight.id, pomDate, pomStatus);
                airplanesExecuting.put(counter, result);
                counter++;

                int chanceToRedeploy = StaticVariables.random.nextInt(1000);
                //drugie nadanie
                if (chanceToRedeploy < 100) {
                    do {
                        pomAirplane2 = airplanes.ariplanes.get(poms.get(StaticVariables.random.nextInt(poms.size())));

                    } while (pomAirplane.id == pomAirplane2.id);
                    pomStatus = "Drugie nadanie";
                    Date pomScheduldedStartDate = pomCarriedFlight.scheduledFlightStart;
                    long pom = StaticFunctions.nextLong( pomScheduldedStartDate.getTime() - pomDate.getTime() + 1) + pomScheduldedStartDate.getTime();
                    pomDate.setTime(pom);
                    result = new AirplaneExecuting(counter, pomAirplane2.id, pomCarriedFlight.id, pomDate, pomStatus);
                    airplanesExecuting.put(counter, result);
                    counter++;
                }

                chanceToRedeploy = StaticVariables.random.nextInt(1000);

                if (chanceToRedeploy < 10) {
                    if (pomAirplane2 != null) {
                        do {
                            pomAirplane3 = airplanes.ariplanes.get(poms.get(StaticVariables.random.nextInt(poms.size())));

                        } while (pomAirplane2.id == pomAirplane3.id);
                    } else {
                        do {
                            pomAirplane3 = airplanes.ariplanes.get(poms.get(StaticVariables.random.nextInt(poms.size())));

                        } while (pomAirplane.id == pomAirplane3.id);
                    }
                    pomStatus = "Zastepczy";
                    Date pomScheduldedStartDate = pomCarriedFlight.scheduledFlightStart;
                    Date pomStartDate = pomCarriedFlight.flightStart;
                    long pom = StaticFunctions.nextLong( pomStartDate.getTime() - pomScheduldedStartDate.getTime() + 1) + pomStartDate.getTime();
                    pomDate.setTime(pom);
                    result = new AirplaneExecuting(counter, pomAirplane3.id, pomCarriedFlight.id, pomDate, pomStatus);
                    airplanesExecuting.put(counter, result);
                    counter++;
                }
                i++;
            }
        }
    }

}
