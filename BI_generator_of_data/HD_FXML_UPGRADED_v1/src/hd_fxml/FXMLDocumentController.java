/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hd_fxml;

import time.*;
import airplane.Airplanes;
import airplane_executing.AirplanesExecuting;
import airport.Airports;
import flight.Flights;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import client.Countries;
import client.Names;
import client.People;
import client.Surnames;
import carried_flight.CarriedFlights;
import com.sun.corba.se.impl.protocol.giopmsgheaders.Message;
import excel_file.WriteExcelClients;
import excel_file.WriteExcelOperations;
import java.text.ParseException;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import operation.ListOfOperations;
import sql_file.WriteSQL;

/**
 *
 * @author Arkadiusz
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    BorderPane BorderPane;

    @FXML
    TextField startDateTextField;

    @FXML
    TextField midDateTextField;

    @FXML
    TextField endDateTextField;

    @FXML
    Button generateT0T1Button;

    @FXML
    Button generateT1T2Button;

    @FXML
    TextField startAirplanesNumberTextField;

    @FXML
    TextField endAirplanesNumberTextField;

    @FXML
    TextField startConnectionsNumberTextField;

    @FXML
    TextField endConnectionsNumberTextField;

    @FXML
    TextField startUsersNumberTextField;

    @FXML
    TextField endUsersNumberTextField;

    @FXML
    TextField europeProbabilityTextField;

    @FXML
    TextField asiaProbabilityTextField;

    @FXML
    TextField oceaniaProbabilityTextField;

    @FXML
    TextField africaProbabilityTextField;

    @FXML
    TextField northAmericaProbabilityTextField;

    @FXML
    TextField southAmericaProbabilityTextField;

    @FXML
    TextField insideContinentProbabilityTextField;

    @FXML
    TextField betweenContinentProbabilityTextField;

    @FXML
    TextField randomContinentProbabilityTextField;

    @FXML
    TextField startCarriedFlightsTextField;

    @FXML
    TextField endCarriedFlightsTextField;

    private Calendar startCalendar = Calendar.getInstance();
    private Calendar midCalendar = Calendar.getInstance();
    private Calendar endCalendar = Calendar.getInstance();

    private Surnames surnames;
    private Names names;
    private People people;
    private Countries countries;
    private Airports airports;
    private Flights flights;
    private CarriedFlights carriedFlights;
    private Airplanes airplanes;
    private AirplanesExecuting airplanesExecuting;

    int startUsersNumber;
    int endUsersNumber;

    int startAirplanesNumber;
    int endAirplanesNumber;

    int startConnectionsNumber;
    int endConnectionsNumber;

    int startCarriedFlightsNumber;
    int endCarriedFlightsNumber;

    private ListOfOperations listOfOperations;
    WriteExcelClients writeExcelClients;
    WriteSQL writeSQL;
    WriteExcelOperations writeExcelOperations;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        String pathOfNames = "lib_list_files\\names.txt";
        String pathOfSurnames = "lib_list_files\\surnames.txt";

        names = new Names(pathOfNames);
        surnames = new Surnames(pathOfSurnames);
        String pathOfCountries = "lib_list_files\\countries.txt";
        countries = new Countries(pathOfCountries);
        people = new People(names, surnames, countries);
        String pathOfAirports = "lib_list_files\\airports.dat";
        airports = new Airports(pathOfAirports);
        flights = new Flights(airports);
        carriedFlights = new CarriedFlights(flights);
        //    tester = new Tester(); //w tej klasie testowalem zakomentuj jak nie chcesz zeby zajmowala czas
        airplanes = new Airplanes();
        airplanesExecuting = new AirplanesExecuting(airplanes, carriedFlights);
        listOfOperations = new ListOfOperations(carriedFlights, people, airplanesExecuting);
        writeExcelClients = new WriteExcelClients(people.people);
        writeSQL = new WriteSQL();
        writeExcelOperations = new WriteExcelOperations(listOfOperations);
        this.generateT1T2Button.setDisable(true);
    }

    @FXML
    public void generateT1T2(ActionEvent event) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            midCalendar.setTime(formatter.parse(midDateTextField.getText()));
            endCalendar.setTime(formatter.parse(endDateTextField.getText()));

            endUsersNumber = Integer.parseInt(endUsersNumberTextField.getText());

            endAirplanesNumber = Integer.parseInt(endAirplanesNumberTextField.getText());

            endConnectionsNumber = Integer.parseInt(endConnectionsNumberTextField.getText());

            endCarriedFlightsNumber = Integer.parseInt(endCarriedFlightsTextField.getText());

            countries.setDistribution(Double.parseDouble(europeProbabilityTextField.getText()),
                    Double.parseDouble(asiaProbabilityTextField.getText()),
                    Double.parseDouble(oceaniaProbabilityTextField.getText()),
                    Double.parseDouble(africaProbabilityTextField.getText()),
                    Double.parseDouble(northAmericaProbabilityTextField.getText()),
                    Double.parseDouble(southAmericaProbabilityTextField.getText())
            );

            flights.setDistribution(Double.parseDouble(insideContinentProbabilityTextField.getText()),
                    Double.parseDouble(betweenContinentProbabilityTextField.getText()),
                    Double.parseDouble(randomContinentProbabilityTextField.getText())
            );
            people.scdp();
            people.createPeopleFromStart(endUsersNumber - startUsersNumber, midCalendar, endCalendar);
            flights.createFlightsFromStart(endConnectionsNumber - startConnectionsNumber);

            carriedFlights.loadCarriedFlights(endCarriedFlightsNumber - startCarriedFlightsNumber, midCalendar, endCalendar);

            airplanes.createAirplanes(endAirplanesNumber - startAirplanesNumber);

            airplanesExecuting.createAirplanesExecuting();

            listOfOperations.createListOfOperations();

            writeSQL.createSQLFile("2\\createSqlTable.sql");

            writeExcelClients.writeToExcel("2\\Klienci2.xls");
            writeSQL.writeToSQLFileAirplanes("2\\airplanes2.sql", airplanes);
            writeSQL.writeToSQLFileFlights("2\\flights2.sql", flights);
            //writeSQL.writeToSQLFilePassengers("2\\passengers2_", carriedFlights);
            writeSQL.writeToSQLFileCarriedFlights("2\\carriedFlights2.sql", carriedFlights);
            writeSQL.writeToSQLFileAirplanesExecuting("2\\airplanesExecuting2.sql", airplanesExecuting);

            writeExcelOperations.writeToExcel("2\\Operacje2_");
            this.generateT1T2Button.setDisable(true);

        } catch (ParseException | NumberFormatException e) {
            System.out.println(e);
        }
    }

    @FXML
    public void generateT0T1(ActionEvent event) {
        try {
            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

            startCalendar.setTime(formatter.parse(startDateTextField.getText()));
            midCalendar.setTime(formatter.parse(midDateTextField.getText()));

            DataCreate dataPom = new DataCreate(startCalendar, endCalendar);
            dataPom.CreateDataTxt();

            TimeCreate timePom = new TimeCreate();
            timePom.CreateTimeTxt();

            startUsersNumber = Integer.parseInt(startUsersNumberTextField.getText());

            startAirplanesNumber = Integer.parseInt(startAirplanesNumberTextField.getText());

            startConnectionsNumber = Integer.parseInt(startConnectionsNumberTextField.getText());

            startCarriedFlightsNumber = Integer.parseInt(startCarriedFlightsTextField.getText());

            countries.setDistribution(Double.parseDouble(europeProbabilityTextField.getText()),
                    Double.parseDouble(asiaProbabilityTextField.getText()),
                    Double.parseDouble(oceaniaProbabilityTextField.getText()),
                    Double.parseDouble(africaProbabilityTextField.getText()),
                    Double.parseDouble(northAmericaProbabilityTextField.getText()),
                    Double.parseDouble(southAmericaProbabilityTextField.getText())
            );

            flights.setDistribution(Double.parseDouble(insideContinentProbabilityTextField.getText()),
                    Double.parseDouble(betweenContinentProbabilityTextField.getText()),
                    Double.parseDouble(randomContinentProbabilityTextField.getText())
            );

            people.createPeopleFromStart(startUsersNumber, startCalendar, midCalendar);
          //  people.writeToTxt(40);

            flights.createFlightsFromStart(startConnectionsNumber);

            carriedFlights.loadCarriedFlights(startCarriedFlightsNumber, startCalendar, midCalendar);

            airplanes.createAirplanes(startAirplanesNumber);

            airplanesExecuting.createAirplanesExecuting();

            listOfOperations.createListOfOperations();

            writeSQL.createSQLFile("1\\createSqlTable.sql");

            writeExcelClients.writeToExcel("1\\Klienci1.xls");
            writeSQL.writeToSQLFileAirports("1\\airports1.sql", airports);
            writeSQL.writeToSQLFileAirplanes("1\\airplanes1.sql", airplanes);
            writeSQL.writeToSQLFileFlights("1\\flights1.sql", flights);

            //writeSQL.writeToSQLFilePassengers("1\\passengers1_", carriedFlights);
            writeSQL.writeToSQLFileCarriedFlights("1\\carriedFlights1.sql", carriedFlights);
            writeSQL.writeToSQLFileAirplanesExecuting("1\\airplanesExecuting1.sql", airplanesExecuting);

            writeExcelOperations.writeToExcel("1\\Operacje1");
            this.generateT0T1Button.setDisable(true);
            this.generateT1T2Button.setDisable(false);
        } catch (ParseException | NumberFormatException e) {
            System.out.println(e);
        }

    }

}
