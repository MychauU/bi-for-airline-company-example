/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airplane;

import utils.Serial;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

/**
 *
 * @author MychauU
 */
public class Airplanes {

    public static int MAX_CAPACITY = 160;

    public Map<String, Airplane> ariplanes;
    Random generator;
    long counter;
    List<AirplaneModel> models;
    Serial serial;

    public Airplanes() {
        ariplanes = new TreeMap<>();
        generator = new Random();
        counter = 1;
        models = new ArrayList<>();
        setModels();
        serial = new Serial(15);
    }

    public void createAirplanes(int quantity) {
        for (int i = 0; i < quantity;) {
            AirplaneModel pomModel = models.get(generator.nextInt(models.size()));
            String serialPom = serial.nextSerial();
            if (ariplanes.get(serialPom) == null) {
                ariplanes.put(serialPom, new Airplane(counter, pomModel.model, pomModel.capacity, serialPom, pomModel.weight, pomModel.width));
                counter++;
                i++;
            }
        }
    }

    private void setModels() {
        this.models.add(new AirplaneModel("Boeing 737-800", 41410, 34, MAX_CAPACITY));
        this.models.add(new AirplaneModel("Boeing 737-700", 37648, 36, MAX_CAPACITY));
        this.models.add(new AirplaneModel("Boeing 737 MAX 200", 60123, 36, MAX_CAPACITY));
    }
}
