/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airplane;

/**
 *
 * @author MychauU
 */
public class Airplane {
    
    public String model;
    public String serial;
    public long id;
    public int capacity;
    public int width;
    public int weight;
    public boolean done;
    
    public Airplane(long id,String model,int capacity,String serial,int weight,int width){
        this.id=id;
        this.model=model;
        this.capacity=capacity;
        this.serial=serial;
        this.weight=weight;
        this.width=width;
        done=false;
    }
}
