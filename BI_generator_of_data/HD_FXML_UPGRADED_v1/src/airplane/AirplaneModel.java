/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airplane;

/**
 *
 * @author MychauU
 */
public class AirplaneModel {

    String model;
    int weight;
    int width;
    int capacity;

    public AirplaneModel(String model, int weight, int width, int capacity) {
        this.model=model;
        this.weight=weight;
        this.width=width;
        this.capacity=capacity;
    }
}
