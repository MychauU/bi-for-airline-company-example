package client;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class Names {

    String pathOfNames;
    public static List<String> names=new ArrayList<>();
    File file;
    int quantity;
    public Names() {

    }

    public Names(String pathOfNames) {
        this.pathOfNames = pathOfNames;
        file = new File(pathOfNames);
        loadNames();
    }

    private void loadNames() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null;) {
                names.add(line);
                // process the line.
            }
            // line is not visible here.
        } catch (IOException ex) {
            Logger.getLogger(Names.class.getName()).log(Level.SEVERE, null, ex);
        }
        quantity=names.size();
    }
    
    public static String getRandomName(){
        String name;
        name=names.get(StaticVariables.random.nextInt(names.size()));
        return name;
    }
}
