/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import utils.Serial;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class Person {
    public String name;
    public String surname;
    public String email;
    public String country;
    public Date  dateRegistry;
    public Date  dateLastFlight;
    public int countBoughtTickets;
    public Date dateLastLogin;
    public Date dateOfCancel;
    public String isAdult;
    long id;
    public String identifierCode;
    public String serialNumber;
    public String typeOfDocument;
    public String clientIdentifierCode;
    public boolean done;
    public List<Person> passengers;
    /**
     * 
     * @param id
     * @param name
     * @param surname
     * @param email
     * @param country
     * @param dateRegistry
     * @param serialNumber
     * @param typeOfDocument
     * @param identifierCode
     * @param clientIdentifierCode 
     */
    public Person(long id,String name,String surname,String email,String country,Date dateRegistry,String serialNumber,String typeOfDocument, String identifierCode,String clientIdentifierCode,String isAdult){
 
        this.passengers = new ArrayList<>();
       // this.id=id;
        this.name=name;
        this.surname=surname;
        this.email=email;
        this.country=country;
        this.dateRegistry=dateRegistry;
        this.dateLastFlight=dateRegistry;
        this.dateLastLogin=dateRegistry;
        this.countBoughtTickets=0;
        this.serialNumber =serialNumber;
        this.typeOfDocument=typeOfDocument;
        this.done=false;
        this.identifierCode = identifierCode;
        this.dateOfCancel = dateRegistry;
        this.clientIdentifierCode=clientIdentifierCode;
        this.isAdult=isAdult;
    }

    public void changeDocument() {
       Serial serial = new Serial(9);
        String serialNumber = serial.nextSerial();
        String typeOfDocument = StaticVariables.typeOfDocument[StaticVariables.random.nextInt(StaticVariables.typeOfDocument.length)];
        this.serialNumber=serialNumber;
        this.typeOfDocument=typeOfDocument;
    }
    
}
