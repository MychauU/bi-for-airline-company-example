package client;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class Surnames {

    String pathOfSurnames;
    public static List<String> surnames = new ArrayList<>();
    File file;
    int quantity;

    public Surnames() {

    }

    public Surnames(String pathOfNames) {
        
        this.pathOfSurnames = pathOfNames;
        file = new File(pathOfNames);
        
        loadSurnames();
    }

    private void loadSurnames() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null;) {
                surnames.add(line);
                // process the line.
            }
            // line is not visible here.
        } catch (IOException ex) {
            Logger.getLogger(Names.class.getName()).log(Level.SEVERE, null, ex);
        }
        quantity=surnames.size();
    }

    public static String getRandomSurname() {
        String surname;
        surname = surnames.get(StaticVariables.random.nextInt(surnames.size()));
        return surname;
    }

}
