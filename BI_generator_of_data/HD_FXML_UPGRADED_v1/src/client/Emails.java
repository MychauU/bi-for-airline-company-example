/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

//import java.text.Normalizer;
//import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import utils.StaticVariables;
/**
 *
 * @author MychauU
 */
public class Emails {



    private static String deAccent(String str) {
        //zwraca string bez akcentow takie jak łąśćż...
        return StringUtils.stripAccents(str);
        //  String xd="ęóąśłżźćń";
        //   String xd2=StringUtils.stripAccents(xd);
        //  String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
        //  Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        //  String pom=pattern.matcher(nfdNormalizedString).replaceAll("ęóąśłżźćń");
    }
/**
 * return email with variations:
 * short or long :
 *      names,surnames
 * include digits or not
 * include colons between name and surname
 * @param name
 * @param surname
 * @return 
 */
    public static String getEmail(String name, String surname) {
        String email;
        String[] endings = {"gmail.com", "yahoo.com", "hotmail.com", "wp.pl", "vp.pl", "o2.pl", "op.pl"};
        String[] colons = {"_", ".", "-"};
        name = deAccent(name);
        surname = deAccent(surname);
        name = name.toLowerCase();
        surname = surname.toLowerCase();

        switch (StaticVariables.random.nextInt(4)) {
            case 0: {
                //short name
                name = name.substring(0, StaticVariables.random.nextInt(name.length()) + 1);
            }
            break;
            case 1: {
                //short surname
                surname = surname.substring(0, StaticVariables.random.nextInt(surname.length()) + 1);
            }
            break;
            case 2: {
                //short both
                name = name.substring(0, StaticVariables.random.nextInt(name.length()) + 1);
                surname = surname.substring(0, StaticVariables.random.nextInt(surname.length()) + 1);
            }
            break;
            case 3: {
                //long both
            }
            break;
            default: {

            }

        }

        switch (StaticVariables.random.nextInt(4)) {

            case 0: {
                //0 - no digits, no colons
                email = name + surname + "@" + endings[StaticVariables.random.nextInt(endings.length)];
            }
            break;
            case 1: {
                //1 - digits, no colons
                String numbers = "";
                for (int i = 0; i < (StaticVariables.random.nextInt(4) + 1); i++) {
                    numbers += StaticVariables.random.nextInt(10);
                }
                email = name + surname + numbers + "@" + endings[StaticVariables.random.nextInt(endings.length)];
            }
            break;
            case 2: {
                //2 - no digits, colons
                email = name + colons[StaticVariables.random.nextInt(colons.length)] + surname + "@" + endings[StaticVariables.random.nextInt(endings.length)];
            }
            break;
            case 3: {
                //3 - digits, colons
                String numbers = "";
                for (int i = 0; i < (StaticVariables.random.nextInt(4) + 1); i++) {
                    numbers += StaticVariables.random.nextInt(10);
                }
                email = name + colons[StaticVariables.random.nextInt(colons.length)] + surname + numbers + "@" + endings[StaticVariables.random.nextInt(endings.length)];
            }
            break;
            default: {
                email = name + colons[StaticVariables.random.nextInt(colons.length)] + surname + "@" + endings[StaticVariables.random.nextInt(endings.length)];
            }

        }

        return email;
    }

}
