/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import utils.DistributedRandomNumberGenerator;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class Countries {
   String pathOfCountries;
    List<Country> countries;
    List <Country> europeCountries;
    List <Country> africaCountries;
    List <Country> northAmericaCountries;
    List <Country> southAmericaCountries;
    List <Country> asiaCountries;
    List <Country> oceaniaCountries;
    File file;
    int quantity;
    DistributedRandomNumberGenerator distributedGenerator;
    
    /**
     * do not use
     */
    public Countries() {

    }

    
    /**
     * construcor need path to file which contains list of countries
     * @param pathOfNames 
     */
    public Countries(String pathOfNames) {
        countries = new ArrayList<>();
        europeCountries = new ArrayList<>();
        africaCountries = new ArrayList<>();
        northAmericaCountries = new ArrayList<>();
        southAmericaCountries = new ArrayList<>();
        asiaCountries = new ArrayList<>();
        oceaniaCountries = new ArrayList<>();
        distributedGenerator=new DistributedRandomNumberGenerator();
        this.pathOfCountries = pathOfNames;
        file = new File(pathOfNames);
        loadCountries();
    }

    private void loadCountries() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            
            for (String line; (line = br.readLine()) != null;) {
                String[] pom=line.split(";");
                CONTINENT continent;
                switch(pom[2]){
                    case "Africa":{
                        africaCountries.add(new Country(pom[0],pom[1],pom[2],CONTINENT.AFRICA));
                        continent=CONTINENT.AFRICA;
                    }
                    break;
                    case "Europe":{
                        europeCountries.add(new Country(pom[0],pom[1],pom[2],CONTINENT.EUROPE));
                        continent=CONTINENT.EUROPE;
                    }
                    break;
                    case "North America":{
                        northAmericaCountries.add(new Country(pom[0],pom[1],pom[2],CONTINENT.NORTH_AMERICA));
                        continent=CONTINENT.NORTH_AMERICA;
                    }
                    break;
                    case "South America":{
                        southAmericaCountries.add(new Country(pom[0],pom[1],pom[2],CONTINENT.SOUTH_AMERICA));
                        continent=CONTINENT.SOUTH_AMERICA;
                    }
                    break;
                    case "Asia":{
                        asiaCountries.add(new Country(pom[0],pom[1],pom[2],CONTINENT.ASIA));
                        continent=CONTINENT.ASIA;
                    }
                    break;
                    case "Oceania":{
                        oceaniaCountries.add(new Country(pom[0],pom[1],pom[2],CONTINENT.OCEANIA));
                        continent=CONTINENT.OCEANIA;
                    }
                    break;
                    default:
                        continent=CONTINENT.EUROPE;
                }
                countries.add(new Country(pom[0],pom[1],pom[2],continent));
                // process the line.
            }
            
            // line is not visible here.
        } catch (IOException ex) {
            Logger.getLogger(Names.class.getName()).log(Level.SEVERE, null, ex);
        }
        quantity=countries.size();
    }
    
    public String getRandomCountry(){
        String name;
        name=countries.get(StaticVariables.random.nextInt(quantity)).name;
        return name;
    }
    
    /**
     * set distributions from which function getDistributedRandomCountry() depends on
     * overall sum of all parameters should be 1 but that is not necessary
     * it can be higher, overall the ratio between parameters will be used
     * legend:
     * 0-europe
     * 1-asia
     * 2-oceania
     * 3-africa
     * 4-northamerica
     * 5-southamerica
     * 
     * @param europe
     * @param asia
     * @param oceania
     * @param africa
     * @param northAmerica
     * @param southAmerica 
     */
    public void setDistribution(double europe,double asia,double oceania,double africa,double northAmerica,double southAmerica){
        if (europe<=0 || asia<=0 || oceania<=0 || africa<=0 || northAmerica<=0 || southAmerica<=0){
            System.out.println("Liczba/y mniejsze lub równe 0 w klasie countries funkcja setDistibution");
        }
        distributedGenerator.setAsNew();
        distributedGenerator.addNumber(CONTINENT.EUROPE.GETVALUE(), europe);
        distributedGenerator.addNumber(CONTINENT.ASIA.GETVALUE(), asia);
        distributedGenerator.addNumber(CONTINENT.OCEANIA.GETVALUE(), oceania);
        distributedGenerator.addNumber(CONTINENT.AFRICA.GETVALUE(), africa);
        distributedGenerator.addNumber(CONTINENT.NORTH_AMERICA.GETVALUE(), northAmerica);
        distributedGenerator.addNumber(CONTINENT.SOUTH_AMERICA.GETVALUE(), southAmerica);
    }
    
    /**
     * Get random country with distributed probability from which continent u will get
     * legend:
     * 0-europe
     * 1-asia
     * 2-oceania
     * 3-africa
     * 4-northamerica
     * 5-southamerica
     * @return 
     */
    public String getDistributedRandomCountry(){
        String name;
        CONTINENT enumValue = CONTINENT.values()[distributedGenerator.getDistributedRandomNumber()];
        switch(enumValue){
            case EUROPE:{
            //    List<Country> europeCoListuntries=countries.stream().filter(p -> p.continentEnum.equals(CONTINENT.EUROPE.GETVALUE())).collect(Collectors.toList());
                name=europeCountries.get(StaticVariables.random.nextInt(europeCountries.size())).name;
            }
            break;
            case ASIA:{
                name=asiaCountries.get(StaticVariables.random.nextInt(asiaCountries.size())).name;
            }
            break;
            case OCEANIA:{
                name=oceaniaCountries.get(StaticVariables.random.nextInt(oceaniaCountries.size())).name;
            }
            break;
            case AFRICA:{
                name=africaCountries.get(StaticVariables.random.nextInt(africaCountries.size())).name;
            }
            break;
            case NORTH_AMERICA:{
                name=northAmericaCountries.get(StaticVariables.random.nextInt(northAmericaCountries.size())).name;
            }
            break;
            case SOUTH_AMERICA:{
                name=southAmericaCountries.get(StaticVariables.random.nextInt(southAmericaCountries.size())).name;
            }
            break;
            default:{
                return getRandomCountry();
            }
        }
        return name;
    }
    /**
     * 0-europe
     * 1-asia
     * 2-oceania
     * 3-africa
     * 4-northamerica
     * 5-southamerica
     */
    private void setDefaultDistribution(){
        distributedGenerator.setAsNew();
        distributedGenerator.addNumber(CONTINENT.EUROPE.GETVALUE(), 0.4d);
        distributedGenerator.addNumber(CONTINENT.ASIA.GETVALUE(), 0.2d);
        distributedGenerator.addNumber(CONTINENT.OCEANIA.GETVALUE(), 0.05d);
        distributedGenerator.addNumber(CONTINENT.AFRICA.GETVALUE(), 0.1d);
        distributedGenerator.addNumber(CONTINENT.NORTH_AMERICA.GETVALUE(), 0.15d);
        distributedGenerator.addNumber(CONTINENT.SOUTH_AMERICA.GETVALUE(), 0.1d);
    }
}
