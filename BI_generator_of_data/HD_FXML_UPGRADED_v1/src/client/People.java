/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import utils.Serial;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import utils.ExpodentialDistributionRandom;
import static utils.StaticFunctions.nextLong;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public class People {

    private static long counter= 1;
    public Map<String, Person> people;
    Names namesPool;
    Surnames surnamesPool;
    Countries countriesPool;
    
    ExpodentialDistributionRandom expodentialRandom;
    Serial serial;
    private static List<String> identifierCodes=new ArrayList<>();

    public People(Names names, Surnames surnames, Countries countries) {
        people = new TreeMap<>();
        namesPool = names;
        surnamesPool = surnames;
        countriesPool = countries;
        serial = new Serial(9);
        expodentialRandom = new ExpodentialDistributionRandom();

    }

    /**
     * tutaj dystrybuanta rosnaca
     *
     * @param quantity
     * @param start
     * @param end
     */
    public void createPeopleSecondPhase(int quantity, Calendar start, Calendar end) {
        Serial serialPom = new Serial(15);
        for (int i = 0; i < quantity;) {
            long pom = expodentialRandom.nextLong(end.getTimeInMillis() - start.getTimeInMillis() + 1) + start.getTimeInMillis();
            Date pomDate = new Date(pom);
            String name = Names.getRandomName();
            String surname = Surnames.getRandomSurname();
            String country = countriesPool.getDistributedRandomCountry();
            String email = Emails.getEmail(name, surname);
            String serialNumber = serial.nextSerial();
            String typeOfDocument = StaticVariables.typeOfDocument[StaticVariables.random.nextInt(2)];
            String identifierPom;

            if (people.get(email) == null) {
                do {
                    identifierPom = serialPom.nextSerial();
                } while (identifierCodes.contains(identifierPom));

                identifierCodes.add(identifierPom);
                people.put(email, new Person(People.counter, name, surname, email, country, pomDate, serialNumber, typeOfDocument, identifierPom,"0","Tak"));
                People.counter++;
                i++;

            }

        }
    }
    
    
    public static Person createPerson(Date from,String parent){
        String[] yesNo = {"Tak", "Nie"};
        String handledName = Names.getRandomName();
        String handledSurname = Surnames.getRandomSurname();
        Serial serial = new Serial(9);
        String serialNumber = serial.nextSerial();
        String typeOfDocument = StaticVariables.typeOfDocument[StaticVariables.random.nextInt(StaticVariables.typeOfDocument.length)];
        String isAdult = yesNo[StaticVariables.random.nextInt(yesNo.length)];
        Serial serialPom = new Serial(15);
        String identifierPom;
        do {
                    identifierPom = serialPom.nextSerial();
        } while (identifierCodes.contains(identifierPom));
        return new Person(People.counter++,handledName,handledSurname,"0","0",from,serialNumber,typeOfDocument, identifierPom,parent,isAdult);
    }
/*
    public void writeToTxt(int number) {
        try {
            PrintWriter writer = new PrintWriter("C:\\Users\\Arkadiusz\\Desktop\\persons.txt", "UTF-8");
            Set<String> keySet = people.keySet();
            StringBuilder row = new StringBuilder();
            int counter = 0;
             SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            writer.println("INSERT INTO Osoba ( ID_osoby, ID_uzytkownika , Data_dolaczenia,Data_wygasniecia,Data_ostatniego_lotu,Imie_nazwisko,Kod_identyfikujacy,Typ_dokumentu, Numer_dokumentu) VALUES");
            for (String key : keySet) {
                Person personPom = people.get(key);

                row.append("( ")
                        .append( personPom.id  + ", ")
                        .append(personPom.id + ", ")
                        .append("'" + formater.format(personPom.dateRegistry) + "'" + ", ")
                        .append("'" + formater.format(personPom.dateOfCancel) + "'" + ", ")
                        .append("'" + formater.format(personPom.dateLastFlight) + "'" + ", ")
                        .append("'" + personPom.name + " " + personPom.surname + "'" + ", ")
                        .append("'" + personPom.identifierCode + "'" + ", ")
                        .append("'" + personPom.typeOfDocument + "'" + ", ")
                        .append("'" + personPom.serialNumber + "'" + " ")
                        .append("),");
                writer.println(row);
                row = new StringBuilder();
                counter++;

                if (counter == number) {
                    break;
                }

            }
            writer.close();
        } catch (Exception e) {

        }

    }
*/
    
    
    
    
    /**
     * tutaj jest bum np 1/3 uzytkownikow w 1 miesiacu stworzona a pozniej juz
     * dystrybuanta rosnaca
     *
     * @param quantity
     * @param start
     * @param end
     */
    public void createPeopleFromStart(int quantity, Calendar start, Calendar end) {
        double oneToThree = ((double) quantity) / 3.0;
        int firstStep = (int) Math.floor(oneToThree);
        int secondStep = (int) Math.ceil(((double) quantity) - oneToThree);
        Calendar startPlusOneMonth = Calendar.getInstance();
        startPlusOneMonth.setTime(start.getTime());
        startPlusOneMonth.add(Calendar.MONTH, 1);
        Person pomPerson;
        Serial serialPom = new Serial(15);

        for (int i = 0; i < firstStep;) {
            long pom = nextLong( startPlusOneMonth.getTimeInMillis() - start.getTimeInMillis() + 1) + start.getTimeInMillis();
            Date pomDate = new Date(pom);
            String name = namesPool.getRandomName();
            String surname = surnamesPool.getRandomSurname();
            String country = countriesPool.getDistributedRandomCountry();
            String email = Emails.getEmail(name, surname);
            String serialNumber = serial.nextSerial();
            String typeOfDocument = StaticVariables.typeOfDocument[StaticVariables.random.nextInt(2)];
            String identifierPom;

            if (people.get(email) == null) {
                do {
                    identifierPom = serialPom.nextSerial();
                } while (identifierCodes.contains(identifierPom));

                identifierCodes.add(identifierPom);
                people.put(email, new Person(counter, name, surname, email, country, pomDate, serialNumber, typeOfDocument, identifierPom,"0","Tak"));
                counter++;
                i++;

            }

        }

        for (int i = 0; i < secondStep;) {
            long pom = expodentialRandom.nextLong(end.getTimeInMillis() - start.getTimeInMillis() + 1) + start.getTimeInMillis();
            Date pomDate = new Date(pom);
            String name = namesPool.getRandomName();
            String surname = surnamesPool.getRandomSurname();
            String country = countriesPool.getDistributedRandomCountry();
            String email = Emails.getEmail(name, surname);
            String serialNumber = serial.nextSerial();
            String typeOfDocument = StaticVariables.typeOfDocument[StaticVariables.random.nextInt(2)];
            String identifierPom;
            if (people.get(email) == null) {
                do {
                    identifierPom = serialPom.nextSerial();
                } while (identifierCodes.contains(identifierPom));

                identifierCodes.add(identifierPom);
                people.put(email, new Person(counter, name, surname, email, country, pomDate, serialNumber, typeOfDocument, identifierPom,"0","Tak"));
                counter++;
                i++;

            }
        }
    }
    public void scdp(){
        int probability;
        for (Person x:this.people.values()){
            probability= StaticVariables.random.nextInt(1000);
            if (probability < 20) {
                x.changeDocument();
            }
        }
    }

}
