/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

/**
 *
 * @author MychauU
 */
public class Country {
    String abbreviation;
    String name;
    String continentBelong;
    CONTINENT continentEnum;

    public Country(String abbreviation,String name,String continentBelong,CONTINENT continentEnum) {
       this.abbreviation=abbreviation;
       this.name=name;
       this.continentBelong=continentBelong;
       this.continentEnum=continentEnum;
    }

    
}
