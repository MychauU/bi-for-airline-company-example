/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.util.Date;

/**
 *
 * @author MychauU
 */
public class Passager {
    
    public int id;
    public Date dateAdded;
    public long idCarriedFlight;
    public String name;
    public String surname;
    public String  isAdult;
    public String  typeOfDocument;
    public String serialNumber;
    public String status;
    
    public Passager(int id, Date dateAdded, long idCarriedFlight, String name, String surname, String isAdult, String typeOfDocument, String serialNumber, String status) {
        this.id = id;
        this.dateAdded = dateAdded;
        this.idCarriedFlight = idCarriedFlight;
        this.name = name;
        this.surname = surname;
        this.isAdult = isAdult;
        this.typeOfDocument = typeOfDocument;
        this.serialNumber = serialNumber;
        this.status = status;
    }
}
