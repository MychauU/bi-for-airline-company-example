/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author MychauU
 */
public enum CONTINENT {

    EUROPE(0),
    ASIA(1),
    OCEANIA(2),
    AFRICA(3),
    NORTH_AMERICA(4),
    SOUTH_AMERICA(5);
    private final int continent;
    private static final List<CONTINENT> VALUES= Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static CONTINENT RANDOM_CONTINENT() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
    
    
    private CONTINENT(int continent) {
        this.continent = continent;
    }

    public int GETVALUE() {
        return continent;
    }
}
