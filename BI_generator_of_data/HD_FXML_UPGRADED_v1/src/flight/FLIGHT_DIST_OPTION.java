/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flight;

import airport.TIMEZONE_POSITION;
import static airport.TIMEZONE_POSITION.values;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import utils.StaticVariables;

/**
 *
 * @author MychauU
 */
public enum FLIGHT_DIST_OPTION {
    INSIDE_CONTINENT(0),
    BETWEEN_CONTINENT(1),
    RANDOM_CONTINENT(2),;
    private final int flightOption;
    private static final List<FLIGHT_DIST_OPTION> VALUES= Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();

    public static FLIGHT_DIST_OPTION RANDOM_FLIGHT_DIST_OPTION() {
        return VALUES.get(StaticVariables.random.nextInt(SIZE));
    }

    private FLIGHT_DIST_OPTION(int flightOption) {
        this.flightOption = flightOption;
    }

    public int GETVALUE() {
        return flightOption;
    }
}
