/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flight;

import airport.Airport;
import airport.Airports;
import airport.TIMEZONE_POSITION;
import java.util.Map;
import java.util.TreeMap;
import utils.DistributedRandomNumberGenerator;

/**
 *
 * @author MychauU
 */
public class Flights {

    public Map<Long, Flight> flights;

    Airports airportsPool;
    long counter;
    DistributedRandomNumberGenerator distributedGenerator;
    //   DistributedRandomNumberGenerator distributedGeneratorOptions;

    public Flights(Airports airportsPool) {
        this.flights=new TreeMap<>();
        this.airportsPool = airportsPool;
        counter = 1;
        distributedGenerator = new DistributedRandomNumberGenerator();
        //  distributedGeneratorOptions=new DistributedRandomNumberGenerator();
    }

    public void createFlightsFromStart(int quantity) {
        Flight flight;
        Airport pom1;
        Airport pom2;
        for (int i = 0; i < quantity; i++) {
            FLIGHT_DIST_OPTION enumValue = FLIGHT_DIST_OPTION.values()[distributedGenerator.getDistributedRandomNumber()];
            switch (enumValue) {
                case INSIDE_CONTINENT: {
                    TIMEZONE_POSITION timezone=TIMEZONE_POSITION.RANDOM_TIMEZONE_POSITION();
                    pom1=airportsPool.getRandomTimezoneAirport(timezone);
                    do {
                        pom2=airportsPool.getRandomTimezoneAirport(timezone);
                    }while(pom1.getId()==pom2.getId());
                    flight=new Flight(counter,pom1,pom2);
                    this.flights.put(counter, flight);
                    counter++;
                    
                }
                break;
                case BETWEEN_CONTINENT: {
                    TIMEZONE_POSITION timezone1=TIMEZONE_POSITION.RANDOM_TIMEZONE_POSITION();
                    TIMEZONE_POSITION timezone2;
                    pom1=airportsPool.getRandomTimezoneAirport(timezone1);
                    do {
                        timezone2=TIMEZONE_POSITION.RANDOM_TIMEZONE_POSITION();
                    }while(timezone1.GETVALUE()==timezone2.GETVALUE());
                    pom2=airportsPool.getRandomTimezoneAirport(timezone1);
                    flight=new Flight(counter,pom1,pom2);
                    this.flights.put(counter, flight);
                    counter++;
                }
                break;
                case RANDOM_CONTINENT: {
                    pom1=airportsPool.getRandomAirport();
                    do {
                        pom2=airportsPool.getRandomAirport();
                    }while(pom1.getId()==pom2.getId());
                    flight=new Flight(counter,pom1,pom2);
                    this.flights.put(counter, flight);
                    counter++;
                }
                break;
                default: {

                }

            }

        }
    }

    public void setDistribution(double insideContinent, double betweenContinent, double randomContinent) {
        if (insideContinent <= 0 || betweenContinent <= 0 || randomContinent <= 0) {
            System.out.println("Liczba/y mniejsze lub równe 0 w klasie flights funkcja setDistibutionOption");
        }
        distributedGenerator.setAsNew();
        distributedGenerator.addNumber(FLIGHT_DIST_OPTION.INSIDE_CONTINENT.GETVALUE(), insideContinent);
        distributedGenerator.addNumber(FLIGHT_DIST_OPTION.BETWEEN_CONTINENT.GETVALUE(), betweenContinent);
        distributedGenerator.addNumber(FLIGHT_DIST_OPTION.RANDOM_CONTINENT.GETVALUE(), randomContinent);
    }

}
