/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flight;

import airport.Airport;

/**
 *
 * @author MychauU
 */
public class Flight {

    public Airport from;
    public Airport to;
    public long distance;
    public long id;
    static final double PIx = 3.141592653589793;
    static final double RADIUS = 6378.16;
    public boolean done;
    public Flight(long id, Airport from, Airport to) {
        this.id = id;
        this.from = from;
        this.to = to;
        done=false;
        measureDistance();
    }

    private void measureDistance() {
        double lon1=from.getLongitude();
        double lat1=from.getLatitude();
        double lon2=to.getLongitude();
        double lat2=to.getLatitude();
        double dlon = Math.toRadians(lon2 - lon1);
        double dlat = Math.toRadians(lat2 - lat1);

        double a = (Math.sin(dlat / 2.0) * Math.sin(dlat / 2.0)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * (Math.sin(dlon / 2.0) * Math.sin(dlon / 2.0));
        double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        distance= (long)(angle * RADIUS);
    }
}
