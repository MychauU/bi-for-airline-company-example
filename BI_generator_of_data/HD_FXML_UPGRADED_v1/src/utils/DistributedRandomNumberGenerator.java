package utils;


import java.util.HashMap;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MychauU
 * 
 * @param value
 * @param distribution 
 * 
 * function add number to map with his distibution
 * for example 10 , 0.3 means that probability of getting ten from others numbers
 * is equal to 30%
 * 
 * example:
 * DistributedRandomNumberGenerator drng = new DistributedRandomNumberGenerator();
        drng.addNumber(1, 0.2d);
        drng.addNumber(2, 0.3d);
        drng.addNumber(3, 0.5d);

        int testCount = 1000000;

        HashMap<Integer, Double> test = new HashMap<>();

        for (int i = 0; i < testCount; i++) {
            int random = drng.getDistributedRandomNumber();
            test.put(random, (test.get(random) == null) ? (1d / testCount) : test.get(random) + 1d / testCount);
        }

        System.out.println(test.toString());
        * 
        * shows:{1=0.20019100000017953, 2=0.2999349999988933, 3=0.4998739999935438}
 */
public class DistributedRandomNumberGenerator {

    private  Map<Integer, Double> distribution;
    private double distSum;

    public DistributedRandomNumberGenerator() {
        distribution = new HashMap<>();
        distSum=0;
    }

    public void addNumber(int value, double distribution) {
        if (this.distribution.get(value) != null) {
            distSum -= this.distribution.get(value);
        }
        this.distribution.put(value, distribution);
        distSum += distribution;
    }
    
    public void setAsNew(){
        distribution = new HashMap<>();
        distSum=0;
    }
    
    public int getDistributedRandomNumber() {
        double rand = Math.random();
        if (distSum==0) return 0;
        double ratio = 1.0f / distSum;
        double tempDist = 0;
        for (Integer i : distribution.keySet()) {
            tempDist += distribution.get(i);
            if (rand / ratio <= tempDist) {
                return i;
            }
        }
        return 0;
    }

}

