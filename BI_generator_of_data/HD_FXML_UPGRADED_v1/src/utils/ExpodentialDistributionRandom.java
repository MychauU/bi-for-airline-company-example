/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Random;

/**
 *
 * @author MychauU
 */
public class ExpodentialDistributionRandom {

    double lambda;
    double xmax;
    public ExpodentialDistributionRandom() {
        lambda = 1;
        xmax=0.63212;
    }

    public double nextDouble() {
        double u;
        double x;
        double fx;
        while (true) {
            u = StaticVariables.random.nextDouble();
            x=StaticVariables.random.nextDouble();
            fx = 1-Math.exp(-1*x);
            if (u < fx / xmax) {
                break;
            }
        }
        return  (x);

    }

    public int nextInt(int number) {
        double u;
        double x;
        double fx;
        while (true) {
            u = StaticVariables.random.nextDouble();
            x=StaticVariables.random.nextDouble();
            fx = 1-Math.exp(-1*x);
            if (u < fx / xmax) {
                break;
            }
        }
        return (int) (x * number);

    }
    
    public long nextLong(long number) {
        double u;
        double x;
        double fx;
        while (true) {
            u = StaticVariables.random.nextDouble();
            x=StaticVariables.random.nextDouble();
            fx = 1-Math.exp(-1*x);
            if (u < fx / xmax) {
                break;
            }
        }
        return (long) (x * number);

    }

}
