/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package time;

import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Arkadiusz
 */
public class TimeCreate {

    public TimeCreate() {

    }

    public void CreateTimeTxt() {
        try {
            PrintWriter writer = new PrintWriter("output\\1\\Czas.txt", "UTF-8");

            for (int hour = 0; hour < 24; hour++) {
                for (int min = 0; min < 60; min++) {
                    writer.println(hour + ":" + min);
                }
            }
            writer.close();
        } catch (IOException e) {
           e.printStackTrace();
        }       
    }
}
