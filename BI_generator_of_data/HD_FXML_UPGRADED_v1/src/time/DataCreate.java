/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package time;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Arkadiusz
 */
public class DataCreate {

    private Calendar startData = Calendar.getInstance();
    private Calendar endData= Calendar.getInstance();

    public DataCreate(Calendar start, Calendar end) {
        this.startData.setTime(start.getTime());
        this.endData.setTime(end.getTime());
    }

    public void CreateDataTxt() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startData.getTime());
        
        System.out.println(cal.getTime());
        System.out.println(endData.get(Calendar.MONTH));
        
        int myMonth = endData.get(Calendar.MONTH);
        int myYear = endData.get(Calendar.YEAR);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        
        try{
            PrintWriter writer = new PrintWriter("output\\1\\Daty.txt", "UTF-8");
        
        while (!(myMonth == cal.get(Calendar.MONTH) && myYear == cal.get(Calendar.YEAR))){
            writer.println(format.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        writer.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}
