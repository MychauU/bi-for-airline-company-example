/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airport;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author MychauU
 */
public enum TIMEZONE_POSITION {
    EUROPE(0),
    PACIFIC(1),
    AMERICA(2),
    ATLANTIC(3),
    AFRICA(4),
    INDIAN(5),
    ASIA(6),
    AUSTRALIA(7),
    ANTARCTICA(8);
    private final int timezonePosition;
    private static final List<TIMEZONE_POSITION> VALUES
            = Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static TIMEZONE_POSITION RANDOM_TIMEZONE_POSITION() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }


    private TIMEZONE_POSITION(int timezonePosition) {
        this.timezonePosition = timezonePosition;
    }

    public int GETVALUE() {
        return timezonePosition;
    }
}
