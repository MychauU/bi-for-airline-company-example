/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import client.Names;
import utils.StaticVariables;

/**
 *
 * @author Arkadiusz
 */
public class Airports {

    public List<Airport> airports;
    List<Airport> europeAirports;
    List<Airport> pacificAirports;
    List<Airport> americaAirports;
    List<Airport> atlanticAirports;
    List<Airport> africaAirports;
    List<Airport> indianAirports;
    List<Airport> asiaAirports;
    List<Airport> australiaAirports;
    List<Airport> antarcticaAirports;
    File file;
    int quantity;

    public Airports(String pathOfAirports) {
        airports = new ArrayList<>();
        europeAirports = new ArrayList<>();
        pacificAirports = new ArrayList<>();
        americaAirports = new ArrayList<>();
        atlanticAirports = new ArrayList<>();
        africaAirports = new ArrayList<>();
        indianAirports = new ArrayList<>();
        asiaAirports = new ArrayList<>();
        australiaAirports = new ArrayList<>();
        antarcticaAirports = new ArrayList<>();
        quantity = 1;
        file = new File(pathOfAirports);
        loadAirports();
    }

    private void loadAirports() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null;) {
                String[] pom = line.split(",");

                StringBuilder timezonePositionString = new StringBuilder(pom[11]);
                timezonePositionString.delete((timezonePositionString.indexOf("/")), timezonePositionString.length() - 1);
                timezonePositionString.deleteCharAt(timezonePositionString.length() - 1);
                timezonePositionString.deleteCharAt(0);

                TIMEZONE_POSITION timezonePosition = setTimezonePosition(timezonePositionString.toString());

                // int id = Integer.parseInt(pom[0]);
                double timeZone = Double.parseDouble(pom[9]);

                StringBuilder name = new StringBuilder(pom[1]);
                name.deleteCharAt(0);
                name.deleteCharAt(name.length() - 1);

                StringBuilder country = new StringBuilder(pom[3]);
                country.deleteCharAt(0);
                country.deleteCharAt(country.length() - 1);

                StringBuilder city = new StringBuilder(pom[2]);
                city.deleteCharAt(0);
                city.deleteCharAt(city.length() - 1);

                double longitude = Double.parseDouble(pom[6]);
                double latitude = Double.parseDouble(pom[7]);
                Airport pomAirport = new Airport(quantity, timeZone, name.toString(), city.toString(), country.toString(), longitude, latitude, timezonePosition);
                airports.add(pomAirport);
                addToSpecificList(pomAirport);
                quantity++;
            }

        } catch (Exception ex) {
            System.out.println(quantity);
            Logger.getLogger(Names.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private TIMEZONE_POSITION setTimezonePosition(String timezonePositionString) {
        TIMEZONE_POSITION pom;
        switch (timezonePositionString) {
            case "Europe": {
                pom = TIMEZONE_POSITION.EUROPE;
            }
            break;
            case "Pacific": {
                pom = TIMEZONE_POSITION.PACIFIC;
            }
            break;
            case "America": {
                pom = TIMEZONE_POSITION.AMERICA;
            }
            break;
            case "Atlantic": {
                pom = TIMEZONE_POSITION.ATLANTIC;
            }
            break;
            case "Africa": {
                pom = TIMEZONE_POSITION.AFRICA;
            }
            break;
            case "Indian": {
                pom = TIMEZONE_POSITION.INDIAN;
            }
            break;
            case "Asia": {
                pom = TIMEZONE_POSITION.ASIA;
            }
            break;
            case "Australia": {
                pom = TIMEZONE_POSITION.AUSTRALIA;
            }
            break;
            case "Antarctica": {
                pom = TIMEZONE_POSITION.ANTARCTICA;
            }
            break;
            default:
                pom = TIMEZONE_POSITION.EUROPE;
        }
        return pom;

    }

    private void addToSpecificList(Airport pomAirport) {
        switch (pomAirport.timezonePosition) {
            case EUROPE: {
                europeAirports.add(pomAirport);
            }
            break;
            case PACIFIC: {
                pacificAirports.add(pomAirport);
            }
            break;
            case AMERICA: {
                americaAirports.add(pomAirport);
            }
            break;
            case ATLANTIC: {
                atlanticAirports.add(pomAirport);
            }
            break;
            case AFRICA: {
                africaAirports.add(pomAirport);
            }
            break;
            case INDIAN: {
                indianAirports.add(pomAirport);
            }
            break;
            case ASIA: {
                asiaAirports.add(pomAirport);
            }
            break;
            case AUSTRALIA: {
                australiaAirports.add(pomAirport);
            }
            break;
            case ANTARCTICA: {
                antarcticaAirports.add(pomAirport);
            }
            break;
            default: {

            }
        }
    }

    public Airport getRandomAirport() {
        return this.airports.get(StaticVariables.random.nextInt(airports.size()));
    }

    public Airport getRandomTimezoneAirport(TIMEZONE_POSITION value) {
        Airport pom = null;
        switch (value) {
            case EUROPE: {
                pom = europeAirports.get(StaticVariables.random.nextInt(europeAirports.size()));
            }
            break;
            case PACIFIC: {
                pom = pacificAirports.get(StaticVariables.random.nextInt(pacificAirports.size()));
            }
            break;
            case AMERICA: {
                pom = europeAirports.get(StaticVariables.random.nextInt(europeAirports.size()));
            }
            break;
            case ATLANTIC: {
                pom = atlanticAirports.get(StaticVariables.random.nextInt(atlanticAirports.size()));
            }
            break;
            case AFRICA: {
                pom = africaAirports.get(StaticVariables.random.nextInt(africaAirports.size()));
            }
            break;
            case INDIAN: {
                pom = indianAirports.get(StaticVariables.random.nextInt(indianAirports.size()));
            }
            break;
            case ASIA: {
                pom = asiaAirports.get(StaticVariables.random.nextInt(asiaAirports.size()));
            }
            break;
            case AUSTRALIA: {
                pom=australiaAirports.get(StaticVariables.random.nextInt(australiaAirports.size()));
            }
            break;
            case ANTARCTICA: {
                pom = antarcticaAirports.get(StaticVariables.random.nextInt(antarcticaAirports.size()));
            }
            break;
            default: {
                pom = getRandomAirport();
            }
        }
        return pom;
    }

}
