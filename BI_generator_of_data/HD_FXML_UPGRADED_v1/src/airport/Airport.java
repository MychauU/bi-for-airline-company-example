/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airport;

/**
 *
 * @author Arkadiusz
 */
public class Airport {
    public String city;
    public String name;
    public String country;
    double longitude;
    double latitude;
    public int id;
    public double timezone;
    TIMEZONE_POSITION timezonePosition;

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public int getId() {
        return id;
    }
    
    public Airport(int id, double timezone, String name, String city, String country, double longitude, double latitude,TIMEZONE_POSITION timezonePosition) {
     this.id = id;
     this.timezone = timezone;
     this.city = city;
     this.country=country;
     this.name = name;
     this.latitude =latitude;
     this.longitude = longitude;
     this.timezonePosition=timezonePosition;
    }

}
