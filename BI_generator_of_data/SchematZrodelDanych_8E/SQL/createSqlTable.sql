DROP TABLE Pasazer
DROP TABLE Samolot_Realizujacy
DROP TABLE Realizacja_lotu
DROP TABLE Lot
DROP TABLE Port_lotniczy
DROP TABLE Samolot

CREATE TABLE Port_lotniczy
(
	ID_portu integer PRIMARY KEY,
	Nazwa varchar(80) NOT NULL,
	Miejscowosc varchar(40) NOT NULL,
	Kraj varchar(40) NOT NULL,
	Strefa_czasowa float NOT NULL
);

CREATE TABLE Lot
(
	ID_lotu integer PRIMARY KEY,
	Odleglosc integer NOT NULL,
	ID_port_skad integer NOT NULL REFERENCES Port_lotniczy(ID_portu),
	ID_port_dokad integer NOT NULL REFERENCES Port_lotniczy(ID_portu)
);

CREATE TABLE Samolot
(
	ID_samolotu integer PRIMARY KEY,
	Model varchar(20) NOT NULL,
	Max_miejsc integer NOT NULL,
	Nazwa_seryjna varchar(20) UNIQUE NOT NULL,
	Waga integer NOT NULL,
	Szerokosc integer NOT NULL
);


CREATE TABLE Realizacja_lotu
(
	ID_wyk_lotu integer PRIMARY KEY,
	Data_dodania smalldatetime NOT NULL,
	ID_lotu integer NOT NULL REFERENCES Lot(ID_lotu),
	Data_planowanego_startu smalldatetime NOT NULL,
	Data_planowanego_ladowania smalldatetime NOT NULL,
	Data_startu smalldatetime NOT NULL,
	Data_ladowania smalldatetime NOT NULL,
	Status varchar(20) NOT NULL
	
);

CREATE TABLE Samolot_Realizujacy
(
	ID_samolotu_realizujacego integer PRIMARY KEY,
	ID_samolotu integer NOT NULL REFERENCES Samolot(ID_samolotu),
	Data_nadania_samolotu smalldatetime NOT NULL,
	Status varchar(20) NOT NULL
);

CREATE TABLE Pasazer
(
	ID_osoby integer PRIMARY KEY IDENTITY(1,1),
	Data_dolaczenia smalldatetime NOT NULL,
	ID_realizacji_lotu integer NOT NULL REFERENCES Realizacja_lotu(ID_wyk_lotu),
	Imie varchar(20) NOT NULL,
	Nazwisko varchar(20) NOT NULL,
	Pelnoletnosc varchar(5) NOT NULL,
	Typ_dokumentu varchar(20) NOT NULL,
	Numer_dokumentu varchar(20) NOT NULL,
	Status varchar(40) NOT NULL
);



