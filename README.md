# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

BI system with data in relational database and in excel files. Data warehouse was created based on that data files. 

To populate facts entities, we created generator for this airline example - so it is not based on real data but based on knowledge on airline business. 

Purpose of this project was to learn about BI, data warehouse, ETL process, MDX language, KPI,  multidimensional cube and its representation: ROLAP, MOLAP, HOLAP.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact