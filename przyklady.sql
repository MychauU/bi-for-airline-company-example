use przedsiebiorstwo_lotnicze
INSERT INTO Data
(ID_daty,data,rok,miesiac,dzien_tygodnia) VALUES
	(1,'2006-01-01','2006','stycze�','poniedzia�ek'),
	(2,'2006-01-02','2006','stycze�','wtorek'),
	(3,'2006-01-03','2006','stycze�','�roda'),
	(4,'2006-01-04','2006','stycze�','czwartek'),
	(5,'2006-01-05','2006','stycze�','pi�tek'),
	(6,'2006-01-06','2006','stycze�','sobota'),
	(7,'2006-01-07','2006','stycze�','niedziela'),
	(8,'2006-01-08','2006','stycze�','poniedzia�ek'),
	(9,'2006-01-09','2006','stycze�','wtorek'),
	(10,'2006-01-10','2006','stycze�','�roda'),
	(11,'2006-01-11','2006','stycze�','czwartek'),
	(12,'2006-01-12','2006','stycze�','pi�tek'),
	(13,'2006-01-13','2006','stycze�','sobota'),
	(14,'2006-01-14','2006','stycze�','niedziela'),
	(15,'2006-01-15','2006','stycze�','poniedzia�ek'),
	(16,'2006-01-16','2006','stycze�','wtorek'),
	(17,'2006-01-17','2006','stycze�','�roda'),
	(18,'2006-01-18','2006','stycze�','czwartek'),
	(19,'2006-01-19','2006','stycze�','pi�tek'),
	(20,'2006-01-20','2006','stycze�','sobota'),
	(21,'2006-01-21','2006','stycze�','niedziela'),
	(22,'2006-01-22','2006','stycze�','poniedzia�ek'),
	(23,'2006-01-23','2006','stycze�','wtorek'),
	(24,'2006-01-24','2006','stycze�','�roda'),
	(25,'2006-01-25','2006','stycze�','czwartek'),
	(26,'2006-01-26','2006','stycze�','pi�tek'),
	(27,'2006-01-27','2006','stycze�','sobota'),
	(28,'2006-01-28','2006','stycze�','niedziela'),

	(45,'2006-07-10','2007','listopad','�roda'),
	(46,'2007-08-10','2007','sierpie�','czwartek'),
	(47,'2007-09-10','2007','wrzesie�','pi�tek'),
	(48,'2007-10-10','2007','pa�dziernik','�roda'),
	(49,'2007-11-10','2007','listopad','czwartek'),
	(50,'2007-12-10','2007','grudzie�','pi�tek'),



	(51,'2007-01-10','2007','stycze�','�roda'),
	(52,'2007-02-10','2007','luty','czwartek'),
	(53,'2007-03-10','2007','marzec','pi�tek'),
	(54,'2007-04-10','2007','kwiecie�','�roda'),
	(55,'2007-05-10','2007','maj','czwartek'),
	(56,'2007-06-10','2007','czerwiec','pi�tek'),
	(57,'2007-07-10','2007','lipiec','�roda'),
	(58,'2007-08-10','2007','sierpie�','czwartek'),
	(59,'2007-09-10','2007','wrzesie�','pi�tek'),
	(60,'2007-10-10','2007','pa�dziernik','�roda'),
	(61,'2007-11-10','2007','listopad','czwartek'),
	(62,'2007-12-10','2007','grudzie�','pi�tek'),


	(9999,NULL,'nieznany','nieznany','nieznany');


INSERT INTO Samolot
(
	ID_samolotu,
	Model,
	Nazwa_seryjna
) VALUES
	(1,'Airbus A320-232','ABCDEFGH1234567890A'),
	(2,'Airbus A321-200','ABCDEFGH1234567890B'),
	(3,'Airbus A321Neo','ABCDEFGH1234567890C'),
	(4,'Airbus A320-232','ABCDEFGH1234567890D'),
	(5,'Airbus A321-200','ABCDEFGH1234567890F'),
	(6,'Airbus A321Neo','ABCDEFGH1234567890G'),
	(7,'Airbus A320-232','ABCDEFGH1234567890H'),
	(8,'Airbus A321-200','ABCDEFGH1234567890I'),
	(9,'Airbus A321Neo','ABCDEFGH1234567890J'),
	(10,'Airbus A320-232','ABCDEFGH1234567890K'),
	(11,'Airbus A321-200','ABCDEFGH1234567890L'),
	(12,'Airbus A321Neo','ABCDEFGH1234567890M'),
	(13,'Airbus A320-232','ABCDEFGH1234567890N'),
	(14,'Airbus A321-200','ABCDEFGH1234567890O'),
	(15,'Airbus A321Neo','ABCDEFGH1234567890P');


INSERT INTO Port_lotniczy
(
	ID_portu   ,
	Nazwa ,
	Miejscowosc ,
	Kraj ,
	Strefa_czasowa 
) VALUES
	(1,'Goroka','Goroka','Papua New Guinea','10 GMT'),
	(2,'Madang','Madang','Papua New Guinea','10 GMT'),
	(3,'Mount Hagen','Mount Hagen','Papua New Guinea','10 GMT'),
	(4,'Nadzab','Nadzab','Papua New Guinea','10 GMT'),
	(5,'Port Moresby Jacksons Intl','Port Moresby','Papua New Guinea','10 GMT'),
	(6,'Wewak Intl','Wewak','Papua New Guinea','10 GMT'),
	(7,'Narsarsuaq','Narssarssuaq','Greenland','-3 GMT'),
	(8,'Nuuk','Godthaab','Greenland','-3 GMT'),
	(9,'Sondre Stromfjord','Sondrestrom','Greenland','-3 GMT'),
	(10,'Thule Air Base','Thule','Greenland','-4 GMT'),
	(11,'Akureyri','Akureyri','Iceland','0 GMT'),
	(12,'Egilsstadir','Egilsstadir','Iceland','0 GMT'),
	(13,'Hornafjordur','Hofn','Iceland','0 GMT'),
	(14,'Husavik','Husavik','Iceland','0 GMT'),
	(15,'Isafjordur','Isafjordur','Iceland','0 GMT');

INSERT INTO Lot
(
	ID_lotu,
	ID_port_skad ,
	ID_port_dokad ,
	rodzaj_odleglosci
	
) VALUES
	(1,1,2,'bliskosi�ny'),
	(2,1,3,'dalekosi�ny'),
	(3,2,3,'bliskosi�ny'),
	(4,2,4,'dalekosi�ny'),
	(5,3,5,'bliskosi�ny'),
	(6,3,6,'dalekosi�ny'),
	(7,3,7,'bliskosi�ny'),
	(8,8,9,'dalekosi�ny'),
	(9,5,10,'bliskosi�ny'),
	(10,5,1,'dalekosi�ny'),
	(11,15,11,'bliskosi�ny'),
	(12,14,10,'dalekosi�ny'),
	(13,13,11,'bliskosi�ny'),
	(14,11,5,'dalekosi�ny'),
	(15,3,7,'bliskosi�ny');

INSERT INTO Typ_operacji
(
	ID_typ_operacji,
	nazwa_operacji
) VALUES
	(1,'kupno biletu'),
	(2,'anulowanie biletu'),
	(3,'zmiana daty biletu'),
	(4,'miejsce pocz�tkowe'),
	(5,'zmieniono miejsce'),
	(6,'baga� podr�czny'),
	(7,'baga� rejestrowany'),
	(8,'du�y baga� podr�czny'),
	(9,'baga� rejestrowany anulowany'),
	(10,'du�y baga� podr�czny anulowany');
	

INSERT INTO Samolot_realizujacy_status
(
	ID_statusu,
	status
) VALUES
	(1,'pierwsze nadanie'),
	(2,'drugie nadanie'),
	(3,'zast�pca');

INSERT INTO Realizacja_lotu_status
(
	ID_statusu,
	status
) VALUES
	(1,'oczekuj�cy'),
	(2,'op�niony'),
	(3,'odwo�any'),
	(4,'zrealizowany');